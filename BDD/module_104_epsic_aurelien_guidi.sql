-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 02 Mai 2020 à 13:23
-- Version du serveur :  5.6.20-log
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `module_104_epsic_aurelien_guidi`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_achat_commande`
--

CREATE TABLE IF NOT EXISTS `t_achat_commande` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'l''ID des utilisateurs',
  `user_name` varchar(255) NOT NULL COMMENT 'Le nom d''utilisateur',
  `status` varchar(255) NOT NULL COMMENT 'le statut de la commande',
  `product_id` int(11) NOT NULL COMMENT 'l''ID du produit',
  `product_name` varchar(255) NOT NULL COMMENT 'le nom du produit',
  `quantity` int(11) NOT NULL COMMENT 'la quantite',
  `price` int(11) NOT NULL COMMENT 'le prix',
  `discount` float NOT NULL COMMENT 'le pourcentage de remise',
  `country` varchar(255) NOT NULL COMMENT 'le pays',
  `region` varchar(255) NOT NULL COMMENT 'la region',
  `address` varchar(255) NOT NULL COMMENT 'l''adresse',
  `phone_number` varchar(255) NOT NULL COMMENT 'le numero de telephone',
  `comments` text NOT NULL COMMENT 'les commentaires',
  `files` text NOT NULL COMMENT 'les fichier joints',
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'la date de la commande'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='acheter des commandes' AUTO_INCREMENT=9 ;

--
-- Contenu de la table `t_achat_commande`
--

INSERT INTO `t_achat_commande` (`id`, `user_id`, `user_name`, `status`, `product_id`, `product_name`, `quantity`, `price`, `discount`, `country`, `region`, `address`, `phone_number`, `comments`, `files`, `order_date`) VALUES
(7, 1, 'celestice', 'Accepté', 10, 'Microsoft Surface', 2, 2500, 500, 'suisse', 'geneve', 'route de geneve 78', '6666666666', 'veuillez m''appeler lors de l''envoie svp', 'surface.jpg', '2020-05-02 13:03:20'),
(8, 1, 'celestice', 'en attente', 12, 'Server HP', 1, 9500, 0, 'suisse', 'geneve', 'Route de geneve 77', '222222222222', 'merci pour votre shop online', 'server_HP.jpg', '2020-05-02 13:05:48');

-- --------------------------------------------------------

--
-- Structure de la table `t_avis_produit`
--

CREATE TABLE IF NOT EXISTS `t_avis_produit` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'l''ID des utilisateurs',
  `user_name` varchar(255) NOT NULL COMMENT 'Le nom d''utilisateur',
  `product_id` int(11) NOT NULL COMMENT 'l''ID du produit',
  `product_name` varchar(255) NOT NULL COMMENT 'le nom du produit',
  `rate` tinyint(2) NOT NULL COMMENT 'les avis',
  `review` text NOT NULL COMMENT 'les commentaires',
  `review_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date du commentaire'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='commentaires' AUTO_INCREMENT=8 ;

--
-- Contenu de la table `t_avis_produit`
--

INSERT INTO `t_avis_produit` (`id`, `user_id`, `user_name`, `product_id`, `product_name`, `rate`, `review`, `review_date`) VALUES
(6, 6, 'test502', 9, 'ASUS GAMING', 4, 'tres satisfait de se produit', '2020-05-02 10:58:33'),
(7, 1, 'celestice', 9, 'ASUS GAMING', 1, 'je ne suis pas content du produit', '2020-05-02 10:58:56');

-- --------------------------------------------------------

--
-- Structure de la table `t_avis_produit_slider`
--

CREATE TABLE IF NOT EXISTS `t_avis_produit_slider` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'l''ID des utilisateurs',
  `user_name` varchar(255) NOT NULL COMMENT 'Le nom d''utilisateur',
  `product_id` int(11) NOT NULL COMMENT 'l''ID du produit',
  `product_name` varchar(255) NOT NULL COMMENT 'le nom du produit',
  `rate` tinyint(2) NOT NULL COMMENT 'les notations des clients',
  `review` text NOT NULL COMMENT 'les commentaires',
  `review_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date du commentaire'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='commentaires sliders' AUTO_INCREMENT=4 ;

--
-- Contenu de la table `t_avis_produit_slider`
--

INSERT INTO `t_avis_produit_slider` (`id`, `user_id`, `user_name`, `product_id`, `product_name`, `rate`, `review`, `review_date`) VALUES
(3, 1, 'celestice', 1, 'QNAP NAS', 5, 'excellent produit je le recommande ', '2020-05-02 13:11:15');

-- --------------------------------------------------------

--
-- Structure de la table `t_categorie`
--

CREATE TABLE IF NOT EXISTS `t_categorie` (
`id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL COMMENT 'categorie',
  `number_of_products` int(11) NOT NULL COMMENT 'numero du produit'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='categories' AUTO_INCREMENT=6 ;

--
-- Contenu de la table `t_categorie`
--

INSERT INTO `t_categorie` (`id`, `category`, `number_of_products`) VALUES
(1, 'nas', 2),
(2, 'server', 1),
(3, 'ordinateur', 1),
(4, 'laptop', 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_commande`
--

CREATE TABLE IF NOT EXISTS `t_commande` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'l''ID des utilisateurs',
  `user_name` varchar(255) NOT NULL COMMENT 'Le nom d''utilisateur',
  `status` varchar(255) NOT NULL COMMENT 'statut de la commande',
  `product_id` int(11) NOT NULL COMMENT 'l''ID du produit',
  `product_name` varchar(255) NOT NULL COMMENT 'le nom du produit',
  `quantity` int(11) NOT NULL COMMENT 'la quantite',
  `price` int(11) NOT NULL COMMENT 'Le prix',
  `discount` float NOT NULL COMMENT 'remise',
  `files` text NOT NULL COMMENT 'les fichier joints',
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date de commandes'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='les commandes' AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_contacter_nous`
--

CREATE TABLE IF NOT EXISTS `t_contacter_nous` (
`id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL COMMENT 'statut',
  `username` varchar(255) NOT NULL COMMENT 'le pseudo du client',
  `phone` varchar(255) NOT NULL COMMENT 'numero telephone du client',
  `email` varchar(255) NOT NULL COMMENT 'l''email du client',
  `message` text NOT NULL COMMENT 'le message du client',
  `write_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'la date du message'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='nous contacter' AUTO_INCREMENT=4 ;

--
-- Contenu de la table `t_contacter_nous`
--

INSERT INTO `t_contacter_nous` (`id`, `status`, `username`, `phone`, `email`, `message`, `write_date`) VALUES
(3, 'pas vue', 'Aurelien', '999999999999', 'test150@test.ch', 'Bonjour,Allez vous recevoir bientot des NAS de la marque WD ?', '2020-05-02 13:07:20');

-- --------------------------------------------------------

--
-- Structure de la table `t_produit`
--

CREATE TABLE IF NOT EXISTS `t_produit` (
`id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL COMMENT 'la categorie du produit',
  `number_of_sales` int(11) NOT NULL COMMENT 'nombre de ventes',
  `number_of_views` int(11) NOT NULL COMMENT 'nombre de vue',
  `avg_rate` float NOT NULL COMMENT 'les avis',
  `product_name` varchar(255) NOT NULL COMMENT 'le nom du produit',
  `description` text NOT NULL COMMENT 'La description du produit',
  `price` int(11) NOT NULL COMMENT 'Le prix du produit',
  `discount` float NOT NULL COMMENT 'la remise du produit',
  `quantity` int(11) NOT NULL COMMENT 'la quantite',
  `files` text NOT NULL COMMENT 'les fichier joints',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'la date de creation'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='produits' AUTO_INCREMENT=14 ;

--
-- Contenu de la table `t_produit`
--

INSERT INTO `t_produit` (`id`, `category`, `number_of_sales`, `number_of_views`, `avg_rate`, `product_name`, `description`, `price`, `discount`, `quantity`, `files`, `create_date`) VALUES
(8, 'nas', 0, 0, 0, 'WD EX4', 'capacite max de 16 to , nas de 4 baie', 1500, 0, 150, 'nas_wd.jpg', '2020-05-02 10:48:32'),
(9, 'laptop', 0, 22, 2.5, 'ASUS GAMING', 'laptop tres puissant pour le jeux video', 4500, 0, 15, 'gaming_laptop.jpg', '2020-05-02 10:50:35'),
(10, 'laptop', 1, 3, 0, 'Microsoft Surface', 'avec ecran tactile , ce laptop puissant de microsoft vous plaira', 2500, 500, 58, 'surface.jpg', '2020-05-02 10:56:28'),
(11, 'nas', 0, 0, 0, 'QNAP nas', 'petit nas dee famille de la marque qnap', 6030, 0, 62, 'qnap.jpg', '2020-05-02 12:52:13'),
(12, 'server', 1, 1, 0, 'Server HP', 'server extra puissant pour entreprise de la marque hp', 9500, 0, 33, 'server_HP.jpg', '2020-05-02 12:53:32'),
(13, 'ordinateur', 0, 3, 0, 'ASUS ordinateur bureau', 'ordinateur de bureau', 1150, 0, 250, 'HP_bureau.jpg', '2020-05-02 12:56:47');

-- --------------------------------------------------------

--
-- Structure de la table `t_produit_slider`
--

CREATE TABLE IF NOT EXISTS `t_produit_slider` (
`id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL COMMENT 'la categorie du produit',
  `number_of_sales` int(11) NOT NULL COMMENT 'nombre de ventes',
  `number_of_views` int(11) NOT NULL COMMENT 'nombre de vue',
  `avg_rate` float NOT NULL COMMENT 'les notations des clients',
  `product_name` varchar(255) NOT NULL COMMENT 'le nom du produit',
  `description` text NOT NULL COMMENT 'La description du produit',
  `price` int(11) NOT NULL COMMENT 'Le prix du produit',
  `discount` float NOT NULL COMMENT 'la remise du produit',
  `quantity` int(11) NOT NULL COMMENT 'la quantite',
  `files` text NOT NULL COMMENT 'les fichier joints',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'la date de creation'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='slider produits' AUTO_INCREMENT=4 ;

--
-- Contenu de la table `t_produit_slider`
--

INSERT INTO `t_produit_slider` (`id`, `category`, `number_of_sales`, `number_of_views`, `avg_rate`, `product_name`, `description`, `price`, `discount`, `quantity`, `files`, `create_date`) VALUES
(1, 'nas', 0, 20, 5, 'QNAP NAS', 'nas d''une puissance extreme', 25000, 0, 40, 'TS-1685_PR595_fr.jpg', '2020-04-20 09:55:15'),
(2, 'ordinateur', 0, 0, 0, 'TITAN PC GAMING', 'tres puissant pc gaming', 7900, 0, 20, 'maxresdefault.jpg', '2020-04-20 09:56:17'),
(3, 'server', 0, 1, 0, 'DELL ProLiant', 'tres puissant server dell', 9999, 0, 50, 'hp-proliant-servers.png', '2020-04-20 09:57:42');

-- --------------------------------------------------------

--
-- Structure de la table `t_utilisateurs`
--

CREATE TABLE IF NOT EXISTS `t_utilisateurs` (
`id` int(11) NOT NULL,
  `permission` varchar(10) NOT NULL COMMENT 'les permissions de l''utilisateur',
  `first_name` varchar(100) NOT NULL COMMENT 'le Prenom',
  `last_name` varchar(100) NOT NULL COMMENT 'Le nom',
  `email` varchar(100) NOT NULL COMMENT 'l''email du client',
  `gender` varchar(10) NOT NULL COMMENT 'le sexe du client',
  `country` varchar(50) NOT NULL COMMENT 'le pays ou vie le client',
  `username` varchar(100) NOT NULL COMMENT 'le pseudo du client',
  `password` varchar(100) NOT NULL COMMENT 'le mot de passe du client',
  `reset_password_permission` varchar(12) NOT NULL,
  `reset_password_random` varchar(255) NOT NULL,
  `files` text NOT NULL COMMENT 'photo du client',
  `register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'la date de creation du client'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='utilisateurs' AUTO_INCREMENT=8 ;

--
-- Contenu de la table `t_utilisateurs`
--

INSERT INTO `t_utilisateurs` (`id`, `permission`, `first_name`, `last_name`, `email`, `gender`, `country`, `username`, `password`, `reset_password_permission`, `reset_password_random`, `files`, `register_date`) VALUES
(1, 'user', 'aurelien', 'guidi', 'celestice44@gmail.com', 'Homme', 'suisse', 'celestice', '$5$rounds=535000$k72KXZzYxypGS7jy$musk6DRgvkxwXy1ODVmPhaZ39zJx4.QbbxewovxaDTA', 'no_reset', '', '75341292_10159030196217892_8182993437205725184_n.jpg', '2020-04-20 07:19:59'),
(2, 'admin', 'admin', 'admin', 'admin@admin.ch', 'Homme', 'suisse', 'admin', '$5$rounds=535000$BW16qg1dEKbYsKSs$MagR9UyfFJtK.QcG2KGornkpKRdVHHzE6p6OvBzcJD1', 'no_reset', '', 'admin.png', '2020-04-20 07:21:12'),
(6, 'user', 'sebastien', 'test', 'stest@test.ch', 'Femme', 'italie', 'test502', '$5$rounds=535000$Gv/KwX39NcyE80Th$EWVr.DmEe7fjIrrrmEFN.D.I8AogHgWemedARF28Am1', 'no_reset', '', 'admin.png', '2020-05-02 10:58:03'),
(7, 'user', 'jessica', 'test', 'test@test.ch', 'Femme', 'suisse', 'jessica', '$5$rounds=535000$EaRGh96Yx/gBfXrD$3igQbmnt16wt8TAbuzWBID9v1iMqKWqZJALHVd4D0N.', 'no_reset', '', 'admin.png', '2020-05-02 13:06:27');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_achat_commande`
--
ALTER TABLE `t_achat_commande`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_avis_produit`
--
ALTER TABLE `t_avis_produit`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_avis_produit_slider`
--
ALTER TABLE `t_avis_produit_slider`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_categorie`
--
ALTER TABLE `t_categorie`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_commande`
--
ALTER TABLE `t_commande`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_contacter_nous`
--
ALTER TABLE `t_contacter_nous`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_produit`
--
ALTER TABLE `t_produit`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_produit_slider`
--
ALTER TABLE `t_produit_slider`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_utilisateurs`
--
ALTER TABLE `t_utilisateurs`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_achat_commande`
--
ALTER TABLE `t_achat_commande`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `t_avis_produit`
--
ALTER TABLE `t_avis_produit`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_avis_produit_slider`
--
ALTER TABLE `t_avis_produit_slider`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_categorie`
--
ALTER TABLE `t_categorie`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_commande`
--
ALTER TABLE `t_commande`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_contacter_nous`
--
ALTER TABLE `t_contacter_nous`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_produit`
--
ALTER TABLE `t_produit`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `t_produit_slider`
--
ALTER TABLE `t_produit_slider`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_utilisateurs`
--
ALTER TABLE `t_utilisateurs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
