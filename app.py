# quelques importations
from flask import Flask, render_template, flash, redirect, url_for, session, request, send_from_directory, jsonify
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, validators, FileField, IntegerField, PasswordField
from passlib.hash import sha256_crypt
from werkzeug.utils import secure_filename
from shutil import rmtree, copyfile, copy2, copy, copyfileobj
from functools import wraps
from flask_mail import Mail, Message
import MySQLdb
import os
import random
import string


app = Flask(__name__)
app.config['SECRET_KEY'] = 'maccaud'

# configuration de l'application

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'module_104_epsic_aurelien_guidi'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
mysql = MySQL(app)


# configuration de l'application pour envoyer des e-mails avec gmail

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USERNAME'] = 'test4444568@gmail.com'
app.config['MAIL_PASSWORD'] = 'jeux_maccaud'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# icône de site Web

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'admin.png', mimetype='image/vnd.microsoft.icon')


# page non trouvée route

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


# ajouter pour comparer l'itinéraire

lis = []
@app.route('/add_to_compare/<id>', methods=['post', 'get'])
def add_to_compare(id):
    if len(lis) <= 3:
        lis.append(id)
        session['ids'] = lis
        print(session['ids'])
        flash('ajouté avec succès pour comparer!', 'success')
        return redirect(request.referrer)
    else:
        flash('pas ajouté car la limite est atteinte (4 produits seulement!)', 'danger')
        return redirect(request.referrer)


# comparer la page

@app.route('/compare')
def compare():
    id1 = session['ids'][0]
    id2 = session['ids'][1]
    id3 = session['ids'][2]
    id4 = session['ids'][3]
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit WHERE id = %s", [id1])
    result1 = cur.fetchone()
    cur.execute("SELECT * FROM t_produit WHERE id = %s", [id2])
    result2 = cur.fetchone()
    cur.execute("SELECT * FROM t_produit WHERE id = %s", [id3])
    result3 = cur.fetchone()
    cur.execute("SELECT * FROM t_produit WHERE id = %s", [id4])
    result4 = cur.fetchone()
    cur.close()
    return jsonify(result1, result2, result3, result4)


# page d'accueil

@app.route('/', methods=['post', 'get'])
def home():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit_slider LIMIT 1")
    slider_products_first = cur.fetchall()
    cur.execute("SELECT * FROM t_produit_slider LIMIT 1 OFFSET 1")
    slider_products_second = cur.fetchall()
    cur.execute("SELECT * FROM t_produit_slider LIMIT 1 OFFSET 2")
    slider_products_third = cur.fetchall()
    cur.execute("SELECT * FROM t_produit ORDER BY id DESC LIMIT 6;")
    latest_products = cur.fetchall()
    cur.execute("SELECT * FROM t_categorie")
    categories = cur.fetchall()
    cur.execute("SELECT * FROM t_produit ORDER BY number_of_views DESC LIMIT 3;")
    recommended_products = cur.fetchall()
    cur.execute("SELECT * FROM t_produit ORDER BY number_of_views DESC LIMIT 3 OFFSET 3")
    recommended_products_second = cur.fetchall()
    cur.close()

    return render_template('home.html', latest_products=latest_products, categories=categories, slider_products_first=slider_products_first, slider_products_second=slider_products_second, slider_products_third=slider_products_third, recommended_products=recommended_products, recommended_products_second=recommended_products_second)


# contactez-nous formateurs

class Contact_us(Form):
    name = StringField('Nom', [validators.DataRequired()])
    mobile_phone = StringField('Numero telephone', [validators.DataRequired()])
    email = StringField('E-mail', [validators.DataRequired()])
    message = TextAreaField('Message', [validators.DataRequired()])


# contactez-nous depuis la page d'accueil

@app.route('/contact_us', methods=['post', 'get'])
def contact_us():
    form = Contact_us(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        mobile_phone = form.mobile_phone.data
        # email = form.email.data
        email = request.form['email']
        message = form.message.data
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO t_contacter_nous(status, username, phone, email, message)\
                     VALUES(%s, %s, %s, %s, %s)", ("pas vue", name, mobile_phone, email, message))
        mysql.connection.commit()
        cur.close()
        flash("Votre message nous a été envoyé avec succès!", "success")
        return redirect(url_for('home'))
    return render_template('contact_us.html', form=form)


# prix de la gamme de produits

@app.route('/products_price_range', methods=['post', 'get'])
def products_price_range():

    min_price = request.form['min_price']
    max_price = request.form['max_price']
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT * FROM t_produit WHERE (price BETWEEN %s AND %s)", [min_price, max_price])
    cur.close()
    if result > 0:
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM t_produit WHERE (price BETWEEN %s AND %s)", [min_price, max_price])
        search_products = cur.fetchall()
        cur.close()
        return render_template('user_search.html', search_products=search_products)
    else:
        flash('Aucun produit trouvé', 'warning')
        return render_template('user_search.html')


# page tous les produits

@app.route('/products/<id>')
def products(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT COUNT(id) FROM t_produit ORDER BY id ASC;")
    pr = cur.fetchone()
    number_of_products = int(pr['COUNT(id)'] / 10)
    off = (int(id) * 10) - 10
    cur.execute("SELECT * FROM t_produit ORDER BY id DESC LIMIT 10 OFFSET %s;", [off])
    all_products = cur.fetchall()
    cur.close()
    return render_template('all_products.html', all_products=all_products, number_of_products=number_of_products)


# page de réinitialisation du mot de passe de l'utilisateur

@app.route('/user_forget_password')
def user_forget_password():
    return render_template('user_forget_password.html')


# envoyer un e-mail avec un lien pour réinitialiser le mot de passe du compte utilisateur

@app.route("/user_forget_password_email", methods=['GET', 'POST'])
def user_forget_password_email():
    if request.form['username_reset'] == '':
        flash('Vous navez pas écrit de nom dutilisateur !', 'warning')
        return render_template('user_forget_password.html')
    user_name = request.form['username_reset']
    cur = mysql.connection.cursor()
    r = cur.execute("SELECT id, email, username FROM t_utilisateurs WHERE username = BINARY %s AND permission='user' ", [user_name])
    res = cur.fetchone()
    if r > 0:
        random_for_reset = "".join([random.choice(string.ascii_letters + string.digits) for i in range(250)])
        email = res['email']
        msg = Message()
        msg.sender = 'test4444568@gmail.com'
        msg.subject = "Reinitialiser votre mot de passe"
        msg.recipients = [email]
        msg.body = "Reinitialiser votre mot de passe : http://localhost:5000/user_reset_password/%s/%s \n message envoyé par l'expéditeur automatique de Flask-Mail!" % (res['id'], random_for_reset)
        mail.send(msg)
        cur = mysql.connection.cursor()
        cur.execute("UPDATE t_utilisateurs SET reset_password_permission = 'reset',  reset_password_random = %s WHERE username = %s AND permission='user'", [random_for_reset, user_name])
        mysql.connection.commit()
        cur.close()
        flash("Le message de réinitialisation a été envoyé à votre adresse e-mail!", "success")
        flash("Merci de consulter vos emails!", "warning")
        return redirect(url_for('home'))
    else:
        cur.close()
        flash("Ce nom d'utilisateur est introuvable!", "warning")
        return redirect(url_for('home'))

# réinitialiser les validateurs de formulaire de mot de passe

class reset_password(Form):
    password = PasswordField('Password',
                             [validators.DataRequired(), validators.Length(min=6, max=100),
                              validators.EqualTo('confirm', message='Passwords do not match')])
    confirm = PasswordField('Confirm Password', [validators.DataRequired()])

# écrire une nouvelle page de mot de passe

@app.route("/user_reset_password/<id>/<random_for_reset>", methods=['GET', 'POST'])
def user_reset_password(id, random_for_reset):
    form = reset_password(request.form)
    if request.method == 'POST' and form.validate():
        cur = mysql.connection.cursor()
        cur.execute("SELECT reset_password_permission, reset_password_random FROM t_utilisateurs WHERE id = %s AND permission='user'", [id])
        permission = cur.fetchone()
        password_permission = permission['reset_password_permission']
        reset_random = permission['reset_password_random']
        if password_permission == 'reset' and random_for_reset == reset_random:
            random_reset = "".join([random.choice(string.ascii_letters + string.digits) for i in range(250)])
            encrypted_password = sha256_crypt.encrypt(str(form.password.data))
            cur.execute("UPDATE t_utilisateurs SET password = %s WHERE id = %s AND permission='user'", [encrypted_password, id])
            cur.execute("UPDATE t_utilisateurs SET reset_password_permission = 'no_reset', reset_password_random = %s WHERE id = %s AND permission='user'", [random_reset, id])
            mysql.connection.commit()
            cur.close()
            flash("Vous avez réussi à changer votre mot de passe maintenant!", "success")
            return redirect(url_for('home'))
        else:
            cur.close()
            flash("Vous avez déjà changé votre mot de passe!", "warning")
            return redirect(url_for('home'))
    return render_template('user_reset_password.html', form=form)


#************************************* partie utilisateur ***********************************************************************************************


# formulaire de validation d'enregistrement des utilisateurs

class RegisterForm(Form):
    first_name = StringField('Prenom', [validators.InputRequired()])
    last_name = StringField('Nom', [validators.InputRequired()])
    username = StringField('Pseudo', [validators.InputRequired()])
    password = PasswordField('Mot de passe',
                             [validators.DataRequired(), validators.Length(min=5, max=100),
                              validators.EqualTo('password', message='mot de passe corresponds pas')])
    confirm = PasswordField('Confirmer mot de passe', [validators.DataRequired()])


# page d'inscription des utilisateurs

@app.route('/user_register', methods=['post', 'get'])
def user_register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data

        folder = os.path.exists(app.root_path + "/static/uploads/users/{}".format(username))
        if folder == True:
            flash('Le nom du dossier existe déjà', 'warning')
            return redirect(url_for('user_register'))

        cur = mysql.connection.cursor()
        cur.execute("SELECT username FROM t_utilisateurs WHERE username = %s", [username])
        res = cur.fetchone()
        if username in str(res):
            cur.close()
            msg = "Ce nom d'utilisateur existe déjà"
            return render_template('user_register.html', form=form, msg=msg)
        else:
            cur.close()
            first_name = form.first_name.data.lower()
            last_name = form.last_name.data.lower()
            email = request.form['email'].lower()
            gender = request.form['gender']
            country = request.form['country']
            username = form.username.data
            password = sha256_crypt.encrypt(str(form.password.data))
            file = request.files['file']
            if file and allowed_file(file.filename):
                try:
                    rmtree(app.root_path + "/static/uploads/users/{}".format(username))
                    os.makedirs(app.root_path + "/static/uploads/users/{}".format(username))
                except:
                    os.makedirs(app.root_path + "/static/uploads/users/{}".format(username))
                filename = secure_filename(file.filename)
                dir = app.root_path + "/static/uploads/users/{}".format(username)
                file.save(os.path.join(dir, filename))
                cur = mysql.connection.cursor()
                cur.execute("INSERT INTO t_utilisateurs(permission, first_name, last_name,\
                             email, gender, country, username, password, reset_password_permission, files)\
                             VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                            ("user", first_name, last_name, email, gender,\
                             country, username, password, 'no_reset', filename))
                mysql.connection.commit()
                cur.close()
                flash('Vous avez créé le compte avec succès!', 'success')
                return redirect(url_for('user_login'))
            elif file.filename == '' or 'file' not in request.files:
                try:
                    rmtree(app.root_path + "/static/uploads/users/{}".format(username))
                    os.makedirs(app.root_path + "/static/uploads/users/{}".format(username))
                except:
                    os.makedirs(app.root_path + "/static/uploads/users/{}".format(username))
                copy(app.root_path + '/static/admin.png', app.root_path + '/static/uploads/users/{}/admin.png'.format(username))
                cur = mysql.connection.cursor()
                cur.execute("INSERT INTO t_utilisateurs(permission, first_name, last_name,\
                                             email, gender, country, username, password, reset_password_permission, files)\
                                             VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                            ("user", first_name, last_name, email, gender, \
                             country, username, password, 'no_reset', 'admin.png'))
                mysql.connection.commit()
                cur.close()
                flash('Vous avez créé le compte avec succès!', 'success')
                return redirect(url_for('user_login'))
    return render_template('user_register.html', form=form)


# page de connexion utilisateur

@app.route('/user_login', methods=['GET', 'POST'])
def user_login():
    if request.method == 'POST':
        username = request.form['username']
        password_candidate = request.form['password']
        cur = mysql.connection.cursor()
        result = cur.execute("SELECT * FROM t_utilisateurs WHERE username = BINARY %s AND permission='user'", [username])
        if result > 0:
            data = cur.fetchone()
            password = data['password']
            if sha256_crypt.verify(password_candidate, password):
                session['user_logged_in'] = True
                session['user_username'] = username
                cur.close()
                flash('Vous êtes maintenant connecté ', 'success')
                return redirect(url_for('user_account'))
            else:
                cur.close()
                error = 'Mauvais mot de passe!'
                return render_template('user_login.html', error=error)
        else:
            cur.close()
            error = 'Le nom dutilisateur est introuvable!'
            return render_template('user_login.html', error=error)
    return render_template('user_login.html')

# vérifier si l'utilisateur est toujours connecté

def is_user_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'user_logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Non autorisé, veuillez vous connecter', 'danger')
            return redirect(url_for('user_login'))
    return wrap

# déconnexion de l'utilisateur

@app.route('/user_logout')
@is_user_logged_in
def user_logout():
    session.clear()
    flash('Vous êtes maintenant déconnecté', 'success')
    return redirect(url_for('user_login'))

# page de compte d'utilisateur

@app.route('/user_account', methods=['post', 'get'])
@is_user_logged_in
def user_account():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_achat_commande WHERE user_name = %s", [session['user_username']])
    orders = cur.fetchall()
    cur.execute("SELECT files FROM t_utilisateurs WHERE username = %s", [session['user_username']])
    image = cur.fetchone()
    user_image = image['files']
    cur.close()
    return render_template('user_account.html', orders=orders, user_image=user_image)

# télécharger une photo de profil utilisateur

@app.route('/user_profile_picture', methods=['post'])
@is_user_logged_in
def user_profile_picture():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part', 'warning')
            return redirect(url_for('user_account'))
        file = request.files['file']
        if file.filename == '':
            flash('You Have to Select a File!', 'warning')
            return redirect(url_for('user_account'))
        if file and allowed_file(file.filename):
            try:
                rmtree(app.root_path + "/static/uploads/users/{}".format(session['user_username']))
                os.makedirs(app.root_path + "/static/uploads/users/{}".format(session['user_username']))
            except:
                os.makedirs(app.root_path + "/static/uploads/users/{}".format(session['user_username']))
            filename = secure_filename(file.filename)
            dir = app.root_path + "/static/uploads/users/{}".format(session['user_username'])
            file.save(os.path.join(dir, filename))
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_utilisateurs SET files = %s WHERE username = %s AND permission = %s;", [filename, session['user_username'], 'user'])
            mysql.connection.commit()
            cur.close()
            flash('Vous avez téléchargé avec succès votre photo de profil!', 'success')
            return redirect(url_for('user_account'))
    return redirect(url_for('user_account'))

# validateurs de formulaire de changement de mot de passe utilisateur

class userchange_password(Form):
    old_password = PasswordField('Ancien mot de passe',
                             [validators.DataRequired(), validators.Length(min=6, max=100)])
    password = PasswordField('Nouveau mot de passe',
                             [validators.DataRequired(), validators.Length(min=6, max=100),
                              validators.EqualTo('confirm', message='Mot de passe non trouve')])
    confirm = PasswordField('Confirmer mot de passe', [validators.DataRequired()])

# page de changement de mot de passe administrateur

@app.route("/user_change_password/", methods=['GET', 'POST'])
@is_user_logged_in
def user_change_password():
    form = userchange_password(request.form)
    if request.method == 'POST' and form.validate():
        cur = mysql.connection.cursor()
        cur.execute("SELECT password FROM t_utilisateurs WHERE username = %s AND permission='user'", [session['user_username']])
        check_password = cur.fetchone()
        cur.close()
        current_password = check_password['password']
        if sha256_crypt.verify(form.old_password.data, current_password):
            cur = mysql.connection.cursor()
            cur.execute("SELECT reset_password_permission, reset_password_random FROM t_utilisateurs WHERE username = %s AND permission='user'", [session['user_username']])
            permission = cur.fetchone()
            cur.close()
            password_permission = permission['reset_password_permission']
            if password_permission == 'reset' or password_permission == 'no_reset':
                random_reset = "".join([random.choice(string.ascii_letters + string.digits) for i in range(250)])
                encrypted_password = sha256_crypt.encrypt(str(form.password.data))
                cur = mysql.connection.cursor()
                cur.execute("UPDATE t_utilisateurs SET password = %s WHERE username = %s AND permission='user'", [encrypted_password, session['user_username']])
                cur.execute("UPDATE t_utilisateurs SET reset_password_permission = 'no_reset', reset_password_random = %s WHERE username = %s AND permission='user'", [random_reset, session['user_username']])
                mysql.connection.commit()
                cur.close()
                session.clear()
                flash("Vous avez réussi à changer votre mot de passe maintenant!", "success")
                return redirect(url_for('user_login'))
        else:
            flash("Vous avez entré un mauvais ancien mot de passe!", "danger")
            return redirect(url_for('user_change_password'))
    return render_template('user_change_password.html', form=form)

# supprimer le compte utilisateur

@app.route('/delete_user_account', methods=['post', 'get'])
@is_user_logged_in
def delete_user_account():
    rmtree(app.root_path + "/static/uploads/users/{}".format(session['user_username']))
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_commande WHERE user_name = %s", [session['user_username']])
    cur.execute("DELETE FROM t_achat_commande WHERE user_name = %s", [session['user_username']])
    cur.execute("DELETE FROM t_avis_produit WHERE user_name = %s", [session['user_username']])
    cur.execute("DELETE FROM t_avis_produit_slider WHERE user_name = %s", [session['user_username']])
    cur.execute("DELETE FROM t_utilisateurs WHERE username = %s", [session['user_username']])
    mysql.connection.commit()
    cur.close()
    session.clear()
    flash('Vous avez supprimé votre compte avec succès!', 'success')
    return redirect(url_for('home'))

# formulaire de validation d'enregistrement des utilisateurs

class CartbuyForm(Form):
    address = StringField('Addresse', [validators.InputRequired(), validators.length(min=10, max=200)])
    phone_number = IntegerField('Numero telelphone', [validators.InputRequired()])
    comments = TextAreaField('Commentaire', [validators.InputRequired()])

# page du panier

@app.route('/add_to_cart', methods=['post', 'get'])
@is_user_logged_in
def add_to_cart():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_commande WHERE user_name = %s", [session['user_username']])
    orders = cur.fetchall()
    cur.execute("SELECT user_name FROM t_commande WHERE user_name = %s", [session['user_username']])
    f = cur.fetchall()
    cur.execute("SELECT SUM((price * quantity) - (quantity * discount)) FROM t_commande WHERE user_name = %s", [session['user_username']])
    order_price = cur.fetchone()
    cur.execute("SELECT SUM(quantity) FROM t_commande WHERE user_name = %s", [session['user_username']])
    quantities = cur.fetchone()
    cur.close()
    return render_template('cart.html', orders=orders, price=order_price['SUM((price * quantity) - (quantity * discount))'], quantity=quantities['SUM(quantity)'], f=f)

# page des commandes d'achat

@app.route('/buy', methods=['post', 'get'])
@is_user_logged_in
def buy():
    cur = mysql.connection.cursor()
    nat = cur.execute("SELECT * FROM t_commande WHERE user_name = %s", [session['user_username']])
    if nat > 0:
        cur.close()
        form = CartbuyForm(request.form)
        if request.method == 'POST' and form.validate():
            cur = mysql.connection.cursor()
            cur.execute("SELECT * FROM t_commande WHERE user_name = %s", [session['user_username']])
            buy_orders = cur.fetchall()
            for order in buy_orders:
                user_id = order['user_id']
                user_name = order['user_name']
                product_id = order['product_id']
                product_name = order['product_name']
                quantity = order['quantity']
                price = order['price']
                discount = order['discount']
                files = order['files']
                cur.execute("INSERT INTO t_achat_commande(user_id, user_name, status, product_id, product_name,\
                                                                quantity, price, discount, files)\
                                                                VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                            (user_id, user_name, 'en attente', product_id, product_name, \
                             quantity, price, discount, files))
                mysql.connection.commit()
            result = cur.execute("SELECT country FROM t_achat_commande WHERE country = '' AND user_name = %s", [session['user_username']])
            if result > 0:
                country = request.form['country']
                region = request.form['region']
                address = form.address.data
                phone_number = form.phone_number.data
                comments = form.comments.data
                cur.execute("UPDATE t_achat_commande SET country = %s, region = %s, address = %s, phone_number = %s, comments = %s WHERE  country = '' AND user_name = %s", \
                    [country, region, address, phone_number, comments, session['user_username']])

                cur.execute("SELECT * FROM t_commande WHERE user_name = %s", [session['user_username']])
                confirm_orders = cur.fetchall()
                for confirm_order in confirm_orders:
                    product_name = confirm_order['product_name']
                    quantity = confirm_order['quantity']
                    cur.execute("UPDATE t_produit SET number_of_sales = number_of_sales + 1 WHERE product_name = %s", [product_name])
                    cur.execute("UPDATE t_produit SET quantity = quantity - %s WHERE product_name = %s", [quantity, product_name])
                    mysql.connection.commit()

                for confir_order in confirm_orders:
                    produc_name = confir_order['product_name']
                    quantity = confir_order['quantity']
                    cur.execute("UPDATE t_produit_slider SET number_of_sales = number_of_sales + 1 WHERE product_name = %s", [produc_name])
                    cur.execute("UPDATE t_produit_slider SET quantity = quantity - %s WHERE product_name = %s", [quantity, produc_name])
                    mysql.connection.commit()

                cur.execute("DELETE FROM t_commande WHERE user_name = %s", [session['user_username']])
                mysql.connection.commit()
                cur.close()
                flash('Votre commande est envoyée avec succès!', 'success')
                return redirect(url_for('home'))
            elif result == 0:
                cur.close()
                flash('vous ne pouvez pas acheter tant que vous najoutez pas de produit à votre panier', 'danger')
                return redirect(url_for('add_to_cart'))
        return render_template('buy.html', form=form)
    elif nat == 0:
        cur.close()
        flash('vous ne pouvez pas acheter tant que vous najoutez pas de produit à votre panier', 'danger')
        return redirect(url_for('add_to_cart'))

# ajouter un produit au panier

@app.route('/add_product_to_cart/<id>', methods=['post', 'get'])
@is_user_logged_in
def add_product_to_cart(id):
    cur = mysql.connection.cursor()

    result = cur.execute("SELECT product_name FROM t_commande WHERE product_id = %s AND user_name = %s ", [id, session['user_username']])
    if result > 0:
        cur.close()
        flash('You can not add this product because its already added before!', 'danger')
        return redirect(url_for('add_to_cart'))
    if result == 0:
        cur.execute("SELECT * FROM t_produit WHERE id = %s", [id])
        product = cur.fetchone()
        product_id = product['id']
        product_name = product['product_name']
        product_price = product['price']
        product_discount = product['discount']
        product_files = product['files']
        user_name = session['user_username']
        cur.execute("SELECT id FROM t_utilisateurs WHERE username = %s", [session['user_username']])
        res = cur.fetchone()
        user_id = res['id']
        cur.execute("INSERT INTO t_commande(user_id, user_name, status, product_id, quantity,\
                                     product_name, price, discount, files)\
                                     VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                    (user_id, user_name, 'En attente', product_id, 1, product_name, \
                     product_price, product_discount, product_files))
        mysql.connection.commit()
        cur.close()
        flash('Ajouté avec succès à votre panier', 'success')
        return redirect(url_for('add_to_cart'))
    return redirect(url_for('home'))

# ajouter un produit au panier à partir du curseur

@app.route('/add_product_to_cart_from_slider/<id>', methods=['post', 'get'])
@is_user_logged_in
def add_product_to_cart_from_slider(id):
    cur = mysql.connection.cursor()

    proid = (int(id) * int(-1))
    result = cur.execute("SELECT product_name FROM t_commande WHERE product_id = %s AND user_name = %s ", [proid, session['user_username']])
    if result > 0:
        cur.close()
        flash('Vous ne pouvez pas ajouter ce produit car il a déjà été ajouté auparavant!', 'danger')
        return redirect(url_for('add_to_cart'))
    if result == 0:
        cur.execute("SELECT * FROM t_produit_slider WHERE id = %s", [id])
        product = cur.fetchone()
        product_id = (int(product['id']) * int(-1))
        product_name = product['product_name']
        product_price = product['price']
        product_discount = product['discount']
        product_files = product['files']
        user_name = session['user_username']
        cur.execute("SELECT id FROM t_utilisateurs WHERE username = %s", [session['user_username']])
        res = cur.fetchone()
        user_id = res['id']
        cur.execute("INSERT INTO t_commande(user_id, user_name, status, product_id, quantity,\
                                     product_name, price, discount, files)\
                                     VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                    (user_id, user_name, 'En attente', product_id, 1, product_name, \
                     product_price, product_discount, product_files))
        mysql.connection.commit()
        cur.close()
        flash('Ajouté avec succès à votre panier', 'success')
        return redirect(url_for('add_to_cart'))
    return redirect(url_for('home'))

# augmenter la quantité de produits du panier dans la page du panier

@app.route('/increase_cart_product_quantity/<id>', methods=['post', 'get'])
@is_user_logged_in
def increase_cart_product_quantity(id):
    cur = mysql.connection.cursor()
    if int(id) > 0:
        cur.execute("SELECT quantity FROM t_produit WHERE id = %s", [id])
        result = cur.fetchone()
        product_quantity = result['quantity']
    elif int(id) < 0:
        fixid = abs(int(id))
        cur.execute("SELECT quantity FROM t_produit_slider WHERE id = %s", [fixid])
        result = cur.fetchone()
        slider_quantity = result['quantity']
    cur.execute("SELECT quantity FROM t_commande WHERE product_id = {}".format(id))
    res = cur.fetchone()
    order_quantity = res['quantity']
    cur.close()
    if int(id) > 0:
        if order_quantity <= product_quantity - 1:
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_commande SET quantity = quantity + 1 WHERE product_id = {}".format(id))
            mysql.connection.commit()
            cur.close()
            flash('Vous avez mis à jour la quantité de produit avec succès!', 'success')
            return redirect(url_for('add_to_cart'))
        else:
            message = 'Vous ne pouvez pas mettre la quantité plus de ' + str(product_quantity) + ' produits!'
            flash(message, 'danger')
            return redirect(url_for('add_to_cart'))
    else:
        if order_quantity <= slider_quantity - 1:
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_commande SET quantity = quantity + 1 WHERE product_id = {}".format(id))
            mysql.connection.commit()
            cur.close()
            flash('Vous avez mis à jour la quantité de produit avec succès!', 'success')
            return redirect(url_for('add_to_cart'))
        else:
            message = 'Vous ne pouvez pas mettre la quantité plus de ' + str(slider_quantity) + ' produits!'
            flash(message, 'danger')
            return redirect(url_for('add_to_cart'))

# modifier la quantité du produit du panier dans la page du panier

@app.route('/edit_cart_product_quantity/<id>', methods=['post', 'get'])
@is_user_logged_in
def edit_cart_product_quantity(id):
    cur = mysql.connection.cursor()
    if int(id) > 0:
        cur.execute("SELECT quantity FROM t_produit WHERE id = %s", [id])
        result = cur.fetchone()
        product_quantity = result['quantity']
    elif int(id) < 0:
        fixid = abs(int(id))
        cur.execute("SELECT quantity FROM t_produit_slider WHERE id = %s", [fixid])
        res = cur.fetchone()
        slider_quantity = res['quantity']
    cur.close()
    quantity = request.form['quantity']
    try:
        if int(quantity) > 0:
            if int(id) > 0:
                if int(quantity) <= product_quantity:
                    cur = mysql.connection.cursor()
                    cur.execute("UPDATE t_commande SET quantity = %s WHERE product_id = %s", [quantity, id])
                    mysql.connection.commit()
                    cur.close()
                    flash('Vous avez mis à jour la quantité de produit avec succès!', 'success')
                    return redirect(url_for('add_to_cart'))
                else:
                    message = 'Vous ne pouvez pas mettre la quantité plus de ' + str(product_quantity) + ' produits!'
                    flash(message, 'danger')
                    return redirect(url_for('add_to_cart'))
            else:
                if int(quantity) <= slider_quantity:
                    cur = mysql.connection.cursor()
                    cur.execute("UPDATE t_commande SET quantity = %s WHERE product_id = %s", [quantity, id])
                    mysql.connection.commit()
                    cur.close()
                    flash('Vous avez mis à jour la quantité de produit avec succès!', 'success')
                    return redirect(url_for('add_to_cart'))
                else:
                    message = 'Vous ne pouvez pas mettre la quantité plus de ' + str(slider_quantity) + ' produits!'
                    flash(message, 'danger')
                    return redirect(url_for('add_to_cart'))
        else:
            pass
            flash('Vous ne pouvez pas mettre la quantité inférieure à un!', 'danger')
            return redirect(url_for('add_to_cart'))
    except ValueError:
        flash('Vous devez écrire un bon nombre!', 'danger')
        return redirect(url_for('add_to_cart'))

# diminuer la quantité de produits du panier dans la page du panier

@app.route('/decrease_cart_product_quantity/<id>', methods=['post', 'get'])
@is_user_logged_in
def decrease_cart_product_quantity(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT quantity FROM t_commande WHERE product_id = %s", [id])
    cart_product = cur.fetchone()
    product_quantity = cart_product['quantity']
    cur.close()
    if product_quantity <= 1:
        flash('Vous ne pouvez pas mettre la quantité inférieure à un!', 'danger')
        pass
    if product_quantity > 1:
        cur.execute("UPDATE t_commande SET quantity = quantity - 1 WHERE product_id = {}".format(id))
        mysql.connection.commit()
        flash('Vous avez mis à jour la quantité de produit avec succès!', 'success')
    return redirect(url_for('add_to_cart'))

# supprimer le produit de la page du panier

@app.route('/delete_product_from_cart/<id>', methods=['post', 'get'])
@is_user_logged_in
def delete_product_from_cart(id):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_commande WHERE product_id = %s", [id])
    mysql.connection.commit()
    cur.close()
    flash('Vous avez supprimé le produit avec succès de votre panier!', 'success')
    return redirect(url_for('add_to_cart'))

# ajouter un avis sur le produit

@app.route('/product_review/<id>', methods=['post', 'get'])
@is_user_logged_in
def product_review(id):
    cur = mysql.connection.cursor()

    result = cur.execute("SELECT product_id FROM t_avis_produit WHERE user_name = %s AND product_id = %s", [session['user_username'], id])
    cur.close()
    if result == 0:
        cur = mysql.connection.cursor()
        cur.execute("SELECT id, product_name FROM t_produit WHERE id = %s", [id])
        product = cur.fetchone()
        product_id = product['id']
        product_name = product['product_name']
        cur.execute("SELECT id, username FROM t_utilisateurs WHERE username = %s", [session['user_username']])
        user = cur.fetchone()
        user_id = user['id']
        user_name = user['username']
        cur.close()

        product_rate = request.form['rate']
        review = request.form['product_review_area']

        if review == '':
            flash('Vous devez écrire un avis!', 'danger')
            return redirect(url_for('home'))
        else:
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO t_avis_produit(user_id, user_name, product_id, product_name, rate, review)\
                         VALUES(%s, %s, %s, %s, %s, %s)", \
                        (user_id, user_name, product_id, product_name, product_rate, review))
            mysql.connection.commit()
            cur.execute("SELECT SUM(rate) / COUNT(product_id) AS total_avg_rate FROM t_avis_produit WHERE product_id = %s", [id])
            total = cur.fetchone()
            total_rate = total['total_avg_rate']
            cur.execute("UPDATE t_produit SET avg_rate = %s WHERE id = %s", [total_rate, id])
            mysql.connection.commit()
            cur.close()
            flash('Votre avis a maintenant été ajouté avec succès!', 'success')
            return redirect(url_for('home'))
    else:
        flash('Vous ne pouvez pas ajouter deux avis pour un produit!', 'danger')
        return redirect(url_for('home'))

# ajouter un avis sur le produit du curseur

@app.route('/slider_product_review/<id>', methods=['post', 'get'])
@is_user_logged_in
def slider_product_review(id):
    cur = mysql.connection.cursor()

    result = cur.execute("SELECT product_id FROM t_avis_produit_slider WHERE user_name = %s AND product_id = %s", [session['user_username'], id])
    cur.close()
    if result == 0:
        cur = mysql.connection.cursor()
        cur.execute("SELECT id, product_name FROM t_produit_slider WHERE id = %s", [id])
        product = cur.fetchone()
        product_id = product['id']
        product_name = product['product_name']
        cur.execute("SELECT id, username FROM t_utilisateurs WHERE username = %s", [session['user_username']])
        user = cur.fetchone()
        cur.close()
        user_id = user['id']
        user_name = user['username']

        product_rate = request.form['rate']
        review = request.form['product_review_area']

        if review == '':
            flash('Vous devez écrire un avis!', 'danger')
            return redirect(url_for('home'))
        else:
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO t_avis_produit_slider(user_id, user_name, product_id, product_name, rate, review)\
                         VALUES(%s, %s, %s, %s, %s, %s)", \
                        (user_id, user_name, product_id, product_name, product_rate, review))
            mysql.connection.commit()
            cur.execute("SELECT SUM(rate) / COUNT(product_id) AS total_avg_rate FROM t_avis_produit_slider WHERE product_id = %s", [id])
            total = cur.fetchone()
            total_rate = total['total_avg_rate']
            cur.execute("UPDATE t_produit_slider SET avg_rate = %s WHERE id = %s", [total_rate, id])
            mysql.connection.commit()
            cur.close()
            flash('Votre avis a maintenant été ajouté avec succès!', 'success')
            return redirect(url_for('home'))
    else:
        flash('Vous ne pouvez pas ajouter deux avis pour un produit!', 'danger')
        return redirect(url_for('home'))


#********************************** Une partie commune entre l'administrateur et l'utilisateur ********************************************************


# aperçu de la page du produit

@app.route('/preview_production/<id>/')
def preview_production(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT COUNT(product_id) FROM t_avis_produit WHERE product_id={}".format(id))
    reviews = cur.fetchone()
    count_reviews = reviews['COUNT(product_id)']
    reviewresult = cur.execute("SELECT * FROM t_avis_produit WHERE product_id={} ORDER BY id DESC".format(id))
    review = cur.fetchall()
    cur.execute("SELECT * FROM t_produit WHERE id={}".format(id))
    product = cur.fetchone()
    cur.execute("SELECT * FROM t_produit WHERE id != %s ORDER BY id DESC LIMIT 6", [id])
    products = cur.fetchall()
    cur.execute("SELECT * FROM t_categorie")
    categories = cur.fetchall()
    cur.execute("UPDATE t_produit SET number_of_views = number_of_views + 1 WHERE id={}".format(id))
    mysql.connection.commit()
    cur.execute("SELECT SUM(rate) / COUNT(product_name) AS avg_rate FROM t_avis_produit WHERE product_id = %s;", [id])
    rate = cur.fetchone()
    cur.close()
    return render_template('preview_production.html', product=product, products=products, categories=categories, count_reviews=count_reviews, review=review, reviewresult=reviewresult, rate=rate)

# aperçu de la page produit du curseur

@app.route('/preview_production_slider/<id>/')
def preview_production_slider(id):
    cur = mysql.connection.cursor()
    slider_reviewresult = cur.execute("SELECT * FROM t_avis_produit_slider WHERE product_id={} ORDER BY id DESC".format(id))
    slider_review = cur.fetchall()
    cur.execute("SELECT COUNT(product_id) FROM t_avis_produit_slider WHERE product_id={}".format(id))
    reviews = cur.fetchone()
    count_reviews = reviews['COUNT(product_id)']
    cur.execute("SELECT SUM(rate) / COUNT(product_name) AS avg_rate FROM t_avis_produit_slider WHERE product_id = %s;", [id])
    rate = cur.fetchone()
    cur.execute("SELECT * FROM t_produit_slider WHERE id={}".format(id))
    product = cur.fetchone()
    cur.execute("SELECT * FROM t_produit_slider WHERE id != %s ORDER BY id DESC LIMIT 6", [id])
    products = cur.fetchall()
    cur.execute("SELECT * FROM t_categorie")
    categories = cur.fetchall()
    cur.execute("UPDATE t_produit_slider SET number_of_views = number_of_views + 1 WHERE id={}".format(id))
    mysql.connection.commit()
    cur.close()
    return render_template('preview_production_slider.html', product=product, products=products, categories=categories, slider_reviewresult=slider_reviewresult, slider_review=slider_review, count_reviews=count_reviews, rate=rate)

# afficher tous les produits dans une catégorie spécifique

@app.route('/categories/<category>', methods=['post', 'get'])
def categories(category):
    cur = mysql.connection.cursor()
    cur.execute("SELECT category, number_of_products FROM t_categorie")
    all_categories = cur.fetchall()
    result = cur.execute("SELECT * FROM t_produit WHERE category=%s", [category])
    categories = cur.fetchall()
    cur.close()
    if result > 0:
        return render_template('catigories.html', categories=categories, all_categories=all_categories)
    else:
        msg = 'No Products Found!'
        return render_template('catigories.html', msg=msg, all_categories=all_categories)

# barre de recherche d'utilisateurs

@app.route('/user_search', methods=['GET', 'POST'])
def user_search():
    if request.method == "POST":
        cur = mysql.connection.cursor()
        result = cur.execute("SELECT * FROM t_produit \
                             WHERE( CONVERT(`product_name` USING utf8)\
                             LIKE %s)", [["%" + request.form['search'] + "%"]])
        search_products = cur.fetchall()
        cur.close()
        if result > 0:
            return render_template('user_search.html', search_products=search_products)
        else:
            flash('Aucun produit trouvé', 'warning')
            return render_template('user_search.html')

#****************************************** partie admin ***********************************************************************************************


# user reset password page

@app.route('/admin/admin_forget_password')
def admin_forget_password():
    return render_template('admin_forget_password.html')


# envoyer un e-mail avec un lien pour réinitialiser le mot de passe du compte utilisateur

@app.route("/admin/admin_forget_password_email", methods=['GET', 'POST'])
def admin_forget_password_email():
    if request.method == 'POST' and request.form['admin_username'] != '':
        user_name = request.form['admin_username']
        cur = mysql.connection.cursor()
        r = cur.execute("SELECT id, email, username FROM t_utilisateurs WHERE username = BINARY %s AND permission='admin' OR permission='editor' ", [user_name])
        res = cur.fetchone()
        cur.close()
        if r > 0:
            random_for_reset = "".join([random.choice(string.ascii_letters + string.digits) for i in range(250)])
            email = res['email']
            msg = Message()
            msg.sender = 'test4444568@gmail.com'
            msg.subject = "Réinitialisez votre mot de passe"
            msg.recipients = [email]
            msg.body = "Réinitialisez votre mot de passe : http://localhost:5000/admin/admin_reset_password/%s/%s \n message envoyé par l'expéditeur automatique de Flask-Mail!" % (res['id'], random_for_reset)
            mail.send(msg)
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_utilisateurs SET reset_password_permission = 'reset',  reset_password_random = %s WHERE username = %s AND permission='admin' OR permission='editor'", [random_for_reset, user_name])
            mysql.connection.commit()
            cur.close()
            flash("Le message de réinitialisation a été envoyé à votre adresse e-mail!", "success")
            flash("Merci de consulter vos emails!", "warning")
            return redirect(url_for('admin_forget_password'))
        else:
            flash("Ce nom d'utilisateur est introuvable!", "warning")
            return redirect(url_for('admin_forget_password'))

# réinitialiser les validateurs de formulaire de mot de passe

class adminreset_password(Form):
    password = PasswordField('',
                             [validators.DataRequired(), validators.Length(min=6, max=100),
                              validators.EqualTo('confirm', message='Les mots de passe ne correspondent pas')])
    confirm = PasswordField('', [validators.DataRequired()])

# écrire une nouvelle page de mot de passe

@app.route("/admin/admin_reset_password/<id>/<random_for_reset>", methods=['GET', 'POST'])
def admin_reset_password(id, random_for_reset):
    form = adminreset_password(request.form)
    if request.method == 'POST' and form.validate():
        cur = mysql.connection.cursor()
        cur.execute("SELECT reset_password_permission, reset_password_random FROM t_utilisateurs WHERE id = %s AND permission='admin' OR permission='editor'", [id])
        permission = cur.fetchone()
        cur.close()
        password_permission = permission['reset_password_permission']
        reset_random = permission['reset_password_random']
        if password_permission == 'reset' and random_for_reset == reset_random:
            random_reset = "".join([random.choice(string.ascii_letters + string.digits) for i in range(250)])
            encrypted_password = sha256_crypt.encrypt(str(form.password.data))
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_utilisateurs SET password = %s WHERE id = %s AND permission='admin' OR permission='editor'", [encrypted_password, id])
            cur.execute("UPDATE t_utilisateurs SET reset_password_permission = 'no_reset', reset_password_random = %s WHERE id = %s AND permission='admin' OR permission='editor'", [random_reset, id])
            mysql.connection.commit()
            cur.close()
            flash("Vous avez réussi à changer votre mot de passe maintenant!", "success")
            return redirect(url_for('admin_login'))
        else:
            flash("Vous avez déjà changé votre mot de passe!", "warning")
            return redirect(url_for('admin_login'))
    return render_template('admin_reset_password.html', form=form)

# page de connexion administrateur

@app.route('/admin/login', methods=['GET', 'POST'])
def admin_login():
    if request.method == 'POST':
        username = request.form['username']
        password_candidate = request.form['password']
        cur = mysql.connection.cursor()
        result = cur.execute("SELECT * FROM t_utilisateurs WHERE username = BINARY %s AND permission='admin' OR permission='editor'", [username])
        if result > 0:
            data = cur.fetchone()
            password = data['password']
            if sha256_crypt.verify(password_candidate, password):
                cur.execute("SELECT files, permission FROM t_utilisateurs WHERE username = %s", [username])
                files = cur.fetchone()
                image = files['files']
                permission = files['permission']
                session['admin_logged_in'] = True
                session['admin_username'] = username
                session['admin_image'] = image
                session['permission'] = permission
                cur.close()
                flash('Vous êtes maintenant connecté ', 'success')
                return redirect(url_for('admin_dashboard'))
            else:
                cur.close()
                error = 'Mauvais mot de passe!'
                return render_template('admin_login.html', error=error)
        else:
            cur.close()
            error = 'Le nom dutilisateur est introuvable!'
            return render_template('admin_login.html', error=error)
    return render_template('admin_login.html')

# vérifier si l'administrateur est toujours connecté 

def is_admin_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'admin_logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Non autorisé, veuillez vous connecter', 'danger')
            return redirect(url_for('admin_login'))
    return wrap

# déconnexion de l'administrateur

@app.route('/admin/logout')
@is_admin_logged_in
def admin_logout():
    session.clear()
    flash('Vous êtes maintenant déconnecté', 'success')
    return redirect(url_for('admin_login'))

# admin changer le mot de passe des validateurs

class adminchange_password(Form):
    old_password = PasswordField('Ancien mot de passe',
                             [validators.DataRequired(), validators.Length(min=6, max=100)])
    password = PasswordField('Nouveau mot de passe',
                             [validators.DataRequired(), validators.Length(min=6, max=100),
                              validators.EqualTo('confirm', message='mot de passe non trouve')])
    confirm = PasswordField('Confirmer mot de passe', [validators.DataRequired()])

# page de changement de mot de passe administrateur

@app.route("/admin/admin_change_password/", methods=['GET', 'POST'])
@is_admin_logged_in
def admin_change_password():
    cur = mysql.connection.cursor()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    form = adminchange_password(request.form)
    if request.method == 'POST' and form.validate():
        cur = mysql.connection.cursor()
        cur.execute("SELECT password FROM t_utilisateurs WHERE username = %s AND permission='admin' OR permission='editor'", [session['admin_username']])
        check_password = cur.fetchone()
        cur.close()
        current_password = check_password['password']
        if sha256_crypt.verify(form.old_password.data, current_password):
            cur = mysql.connection.cursor()
            cur.execute("SELECT reset_password_permission, reset_password_random FROM t_utilisateurs WHERE username = %s AND permission='admin' OR permission='editor'", [session['admin_username']])
            permission = cur.fetchone()
            cur.close()
            password_permission = permission['reset_password_permission']
            if password_permission == 'reset' or password_permission == 'no_reset' or password_permission == '':
                random_reset = "".join([random.choice(string.ascii_letters + string.digits) for i in range(250)])
                encrypted_password = sha256_crypt.encrypt(str(form.password.data))
                cur = mysql.connection.cursor()
                cur.execute("UPDATE t_utilisateurs SET password = %s WHERE username = %s AND permission='admin' OR permission='editor'", [encrypted_password, session['admin_username']])
                cur.execute("UPDATE t_utilisateurs SET reset_password_permission = 'no_reset', reset_password_random = %s WHERE username = %s AND permission='admin' OR permission='editor'", [random_reset, session['admin_username']])
                mysql.connection.commit()
                cur.close()
                session.clear()
                flash("Vous avez réussi à changer votre mot de passe maintenant!", "success")
                return redirect(url_for('admin_login'))
        else:
            flash("Vous avez entré un mauvais ancien mot de passe!", "danger")
            return redirect(url_for('admin_change_password'))
    return render_template('admin_change_password.html', form=form, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# supprimer le compte administrateur

@app.route('/admin/delete_admin_account', methods=['post', 'get'])
@is_admin_logged_in
def delete_admin_account():
    rmtree(app.root_path + "/static/uploads/users/{}".format(session['admin_username']))
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_utilisateurs WHERE username = %s", [session['admin_username']])
    mysql.connection.commit()
    cur.close()
    session.clear()
    flash('Vous avez supprimé votre compte avec succès!', 'success')
    return redirect(url_for('admin_login'))

# page de tableau de bord administrateur

@app.route('/admin/', methods=['post', 'get'])
@is_admin_logged_in
def admin_dashboard():
    cur = mysql.connection.cursor()

    # vérifier si l'administrateur a changé sa photo de profil
    cur.execute("SELECT files, permission FROM t_utilisateurs WHERE username = %s", [session['admin_username']])
    files = cur.fetchone()
    image = files['files']
    session['admin_image'] = image

    # compter les produits de curseur
    cur.execute("SELECT COUNT(id) FROM t_produit_slider")
    sliders = cur.fetchone()
    count_sliders = sliders['COUNT(id)']
    # compter les produits
    cur.execute("SELECT COUNT(id) FROM t_produit")
    products = cur.fetchone()
    count_products = products['COUNT(id)']

    # compter les utilisateurs
    cur.execute("SELECT COUNT(id) FROM t_utilisateurs")
    users = cur.fetchone()
    count_users = users['COUNT(id)']
    # compter les catégories
    cur.execute("SELECT COUNT(category) FROM t_categorie")
    categories = cur.fetchone()
    count_categories = categories['COUNT(category)']

    # compter le nombre de ventes de produits
    cur.execute("SELECT SUM(number_of_sales) FROM t_produit")
    number_of_sales = cur.fetchone()
    count_number_of_sales = number_of_sales['SUM(number_of_sales)']
    # compter le nombre de ventes de produits coulissants
    cur.execute("SELECT SUM(number_of_sales) FROM t_produit_slider")
    number_of_sales = cur.fetchone()
    count_number_of_sales_slider = number_of_sales['SUM(number_of_sales)']

    # montrer le produit où il a un grand nombre de ventes
    cur.execute("SELECT * FROM t_produit ORDER BY number_of_sales DESC LIMIT 1")
    product_saled = cur.fetchone()
    # montrer le produit là où il a un petit nombre de ventes
    cur.execute("SELECT * FROM t_produit ORDER BY number_of_sales ASC LIMIT 1")
    product_saled_low = cur.fetchone()

    # montrer le produit de curseur où il a un grand nombre de ventes
    cur.execute("SELECT * FROM t_produit_slider ORDER BY number_of_sales DESC LIMIT 1")
    slider_saled = cur.fetchone()
    # montrer le produit de curseur où il a un petit nombre de ventes
    cur.execute("SELECT * FROM t_produit_slider ORDER BY number_of_sales ASC LIMIT 1")
    slider_saled_low = cur.fetchone()

    # montrer le produit de curseur où il a un grand nombre de taux
    cur.execute("SELECT * FROM t_avis_produit_slider ORDER BY rate DESC LIMIT 1")
    slider_big = cur.fetchone()
    # afficher le produit slider là où il a un petit nombre de tarifs
    cur.execute("SELECT * FROM t_avis_produit_slider ORDER BY rate ASC LIMIT 1")
    slider_small = cur.fetchone()

    # montrer le produit où il a un grand nombre de tarifs
    cur.execute("SELECT * FROM t_avis_produit ORDER BY rate DESC LIMIT 1")
    product_big = cur.fetchone()
    # montrer le produit là où il a un petit nombre de tarifs
    cur.execute("SELECT * FROM t_avis_produit ORDER BY rate ASC LIMIT 1")
    product_small = cur.fetchone()

    # afficher le nombre de ventes de produit la semaine dernière
    cur.execute("SELECT SUM(number_of_sales) FROM t_produit WHERE create_date >= current_date - 7")
    product_week = cur.fetchone()
    product_last_week = product_week['SUM(number_of_sales)']
    # afficher le nombre de ventes de produits de curseur la semaine dernière
    cur.execute("SELECT SUM(number_of_sales) FROM t_produit_slider WHERE create_date >= current_date - 7")
    slider_week = cur.fetchone()
    slider_last_week = slider_week['SUM(number_of_sales)']

    # afficher le numéro de produit la semaine dernière
    cur.execute("SELECT COUNT(product_name) FROM t_produit WHERE create_date >= current_date - 7")
    product_add_week = cur.fetchone()
    product_add = product_add_week['COUNT(product_name)']
    # afficher le numéro de produit du curseur la semaine dernière
    cur.execute("SELECT COUNT(product_name) FROM t_produit_slider WHERE create_date >= current_date - 7")
    slider_add_week = cur.fetchone()
    slider_add = slider_add_week['COUNT(product_name)']

    # affiche le taux moyen total de tous les avis
    cur.execute("SELECT SUM(rate) / COUNT(rate) AS AVG_RATE FROM (SELECT rate FROM t_avis_produit_slider UNION ALL SELECT rate FROM t_avis_produit) T;")
    avg_rate = cur.fetchone()
    total_avg_rate = avg_rate['AVG_RATE']

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_dashboard.html', count_sliders=count_sliders, count_products=count_products, count_users=count_users, count_categories=count_categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], count_number_of_sales=count_number_of_sales, count_number_of_sales_slider=count_number_of_sales_slider, product_saled=product_saled, slider_saled=slider_saled, slider_big=slider_big, slider_small=slider_small, product_big=product_big, product_small=product_small, slider_add=slider_add, product_add=product_add, slider_last_week=slider_last_week, product_last_week=product_last_week, total_avg_rate=total_avg_rate, product_saled_low=product_saled_low, slider_saled_low=slider_saled_low, messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)


# admin télécharger la page de photo de profil

@app.route('/admin/upload_picture', methods=['post', 'get'])
@is_admin_logged_in
def upload_picture():
    cur = mysql.connection.cursor()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_upload_profile_picture.html', admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# télécharger une photo de profil d'administrateur

@app.route('/admin/admin_profile_picture', methods=['post'])
@is_admin_logged_in
def admin_profile_picture():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('Aucune partie de fichier', 'warning')
            return redirect(url_for('admin_dashboard'))
        file = request.files['file']
        if file.filename == '':
            flash('Vous devez sélectionner un fichier!', 'warning')
            return redirect(url_for('admin_dashboard'))
        if file and allowed_file(file.filename):
            try:
                rmtree(app.root_path + "/static/uploads/users/{}".format(session['admin_username']))
                os.makedirs(app.root_path + "/static/uploads/users/{}".format(session['admin_username']))
            except:
                os.makedirs(app.root_path + "/static/uploads/users/{}".format(session['admin_username']))
            filename = secure_filename(file.filename)
            dir = app.root_path + "/static/uploads/users/{}".format(session['admin_username'])
            file.save(os.path.join(dir, filename))
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_utilisateurs SET files = %s WHERE username = %s AND permission = %s ;", [filename, session['admin_username'], session['permission']])
            mysql.connection.commit()
            cur.close()
            flash('Vous avez téléchargé avec succès votre photo de profil!', 'success')
            return redirect(url_for('admin_dashboard'))
    return redirect(url_for('admin_dashboard'))

# formulaire de validation des produits

class AddProductForm(Form):
    product_name = StringField('Nom du produit', [validators.InputRequired(), validators.length(min=1, max=180)])
    description = TextAreaField('Description', [validators.InputRequired()])
    price = IntegerField('Prix', [validators.InputRequired()])
    discount = StringField('Remise en pourcentage')
    quantity = StringField('Quantite', [validators.InputRequired()])

# admin ajouter une nouvelle page de produit

@app.route('/admin/add_product', methods=['post', 'get'])
@is_admin_logged_in
def add_product():
    form = AddProductForm(request.form)
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT category FROM t_categorie")
    categories = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    if result > 0:
        if request.method == 'POST' and form.validate():
            product_name = form.product_name.data

            folder = os.path.exists(app.root_path + "/static/uploads/products/{}".format(product_name))
            if folder == True:
                flash('Le nom du dossier existe déjà', 'warning')
                return redirect(url_for('add_product'))
            cur = mysql.connection.cursor()
            cur.execute("SELECT product_name FROM t_produit WHERE product_name = %s", [product_name])
            res = cur.fetchone()
            if product_name in str(res):
                msg = "Le nom du produit existe déjà"
                return render_template('admin_add_production.html', form=form, msg=msg, admin_name=session['admin_username'], admin_image=session['admin_image'])

            if request.method == 'POST' and form.validate():
                file = request.files['file']
                if file.filename == '':
                    flash('Vous devez sélectionner un fichier!', 'warning')
                if file and allowed_file(file.filename):
                    try:
                        rmtree(app.root_path + "/static/uploads/products/{}".format(product_name))
                        os.makedirs(app.root_path + "/static/uploads/products/{}".format(product_name))
                    except:
                        os.makedirs(app.root_path + "/static/uploads/products/{}".format(product_name))
                    filename = secure_filename(file.filename)
                    dir = app.root_path + "/static/uploads/products/{}".format(product_name)
                    file.save(os.path.join(dir, filename))
                    category = request.form['categories']
                    description = form.description.data.lower()
                    price = form.price.data
                    discount = form.discount.data
                    quantity = form.quantity.data

                    if discount != '' and discount != ' ':
                        p = round((float(price) * float(discount)) / 100, 2)
                        cur = mysql.connection.cursor()
                        cur.execute("INSERT INTO t_produit(category, product_name, description, price, discount, quantity, files)\
                                                     VALUES(%s, %s, %s, %s, %s, %s, %s)", \
                                    (category, product_name, description, price, p, quantity, filename))
                        cur.execute("UPDATE t_categorie SET number_of_products = number_of_products + 1 WHERE category = %s", [category])
                        mysql.connection.commit()
                        cur.close()
                        flash('Votre produit est publié avec succès!', 'success')
                        return redirect(url_for('admin_dashboard'))

                    if discount == "" or discount == " ":
                        p = 0
                        cur = mysql.connection.cursor()
                        cur.execute("INSERT INTO t_produit(category, product_name, description, price, discount, quantity, files)\
                                                     VALUES(%s, %s, %s, %s, %s, %s, %s)", \
                                    (category, product_name, description, price, p, quantity, filename))
                        cur.execute("UPDATE t_categorie SET number_of_products = number_of_products + 1 WHERE category = %s", [category])
                        mysql.connection.commit()
                        cur.close()
                        flash('Votre produit est publié avec succès!', 'success')
                        return redirect(url_for('admin_dashboard'))

    elif result == 0:
        cur.close()
        flash('Créez dabord une catégorie pour ajouter un nouveau produit', 'warning')
        return redirect(url_for('admin_dashboard'))

    return render_template('admin_add_production.html', form=form, categories=categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin modifier la page du produit

@app.route('/admin/edit_product/<id>', methods=['post', 'get'])
@is_admin_logged_in
def edit_product(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT category FROM t_categorie")
    categories = cur.fetchall()
    cur.close()
    
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit WHERE id={}".format(id))
    product = cur.fetchone()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    form = AddProductForm(request.form)
    form.product_name.data = product['product_name']
    form.description.data = product['description']
    form.price.data = product['price']
    form.quantity.data= product['quantity']
    # form.discount.data = product['discount']
    
    
    d = round((float(product['discount']) * float(100))/float(form.price.data), 2)
    form.discount.data = d
    
    
    if request.method == 'POST' and form.validate():
        product_name = request.form['product_name']

        folder = os.path.exists(app.root_path + "/static/uploads/products/{}".format(product_name))
        if folder == True and form.product_name.data == product_name:
            pass
        elif folder == False and form.product_name.data != product_name:
            pass
        else:
            flash('Folder Name Already Exists', 'warning')
            return redirect(request.url)

        file = request.files['file']
        if file and allowed_file(file.filename):
            rmtree(app.root_path + "/static/uploads/products/{}".format(product['product_name']))
            os.makedirs(app.root_path + "/static/uploads/products/{}".format(product_name))
            filename = secure_filename(file.filename)
            dir = app.root_path + "/static/uploads/products/{}".format(product_name)
            file.save(os.path.join(dir, filename))
        
            category = request.form['categories']
            description = request.form['description']
            price = request.form['price']
            discount = request.form['discount']
            quantity = request.form['quantity']

            if discount == "" or discount == " ":
                p = 0
                cur = mysql.connection.cursor()
                cur.execute("UPDATE t_produit SET category=%s, product_name=%s, description=%s, price=%s,\
                                         discount=%s, quantity=%s, files=%s WHERE id=%s", \
                            (category, product_name, description, price, p, quantity, filename, id))
                mysql.connection.commit()
                cur.close()
                flash('Votre produit a été modifié avec succès!', 'success')
                return redirect(url_for('admin_dashboard'))
            
            p = round((float(price) * float(discount)) / 100, 2)
            
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_produit SET category=%s, product_name=%s, description=%s, price=%s,\
                         discount=%s, quantity=%s, files=%s WHERE id=%s", \
                        (category, product_name, description, price, p, quantity, filename, id))
            mysql.connection.commit()
            cur.close()
            flash('Votre produit a été modifié avec succès!', 'success')
            return redirect(url_for('admin_dashboard'))
        elif file.filename == '' or 'file' not in request.files:
            os.rename(os.path.join(app.root_path + "/static/uploads/products/{}".format(product['product_name'])),
                      os.path.join(app.root_path + "/static/uploads/products/{}".format(product_name)))
            category = request.form['categories']
            description = request.form['description']
            price = request.form['price']
            discount = request.form['discount']
            quantity = request.form['quantity']

            if discount == "" or discount == " ":
                p = 0
                cur = mysql.connection.cursor()
                cur.execute("UPDATE t_produit SET category=%s, product_name=%s, description=%s, price=%s,\
                             discount=%s, quantity=%s WHERE id=%s", \
                            (category, product_name, description, price, p, quantity, id))
                mysql.connection.commit()
                cur.close()
                flash('Votre produit a été modifié avec succès!', 'success')
                return redirect(url_for('admin_dashboard'))
            
            p = round((float(price) * float(discount)) / 100, 2)
            
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_produit SET category=%s, product_name=%s, description=%s, price=%s,\
                         discount=%s, quantity=%s WHERE id=%s", \
                        (category, product_name, description, price, p, quantity, id))
            mysql.connection.commit()
            cur.close()
            flash('Votre produit a été modifié avec succès!', 'success')
            return redirect(url_for('admin_dashboard'))
    return render_template('admin_edit_production.html', form=form, categories=categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin supprimer le produit

@app.route('/admin/delete_product/<id>', methods=['post', 'get'])
@is_admin_logged_in
def delete_product(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT product_name, category FROM t_produit WHERE id = %s", [id])
    name = cur.fetchone()
    n = name['product_name']
    category = name['category']
    try:
        rmtree(app.root_path + "/static/uploads/products/{}".format(n))
    except:
        pass
    cur.execute("DELETE FROM t_produit WHERE id = %s", [id])
    cur.execute("DELETE FROM t_commande WHERE product_id = %s", [id])
    cur.execute("DELETE FROM t_avis_produit WHERE product_id = %s", [id])
    cur.execute("UPDATE t_categorie SET number_of_products = number_of_products - 1 WHERE category = %s", [category])
    mysql.connection.commit()
    cur.close()
    flash('Votre produit a été supprimé avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer tous les produits

@app.route('/admin/delete_all_products', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_products():
    cur = mysql.connection.cursor()
    cur.execute("TRUNCATE t_produit")
    cur.execute("TRUNCATE t_avis_produit")
    mysql.connection.commit()
    cur.close()
    try:
        rmtree(app.root_path + "/static/uploads/products")
        flash('Vous avez déjà supprimé tous les produits avec succès!', 'success')
    except:
        flash('Vous avez déjà supprimé tous les produits avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer le produit

@app.route('/admin/delete_review_products/<id>/<id2>', methods=['post', 'get'])
@is_admin_logged_in
def delete_review_products(id, id2):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_avis_produit WHERE product_id = %s AND id = %s", [id, id2])
    mysql.connection.commit()
    cur.execute("SELECT SUM(rate) / COUNT(product_id) AS total_avg_rate FROM t_avis_produit WHERE product_id = %s", [id])
    total = cur.fetchone()
    total_rate = total['total_avg_rate']
    cur.execute("UPDATE t_produit SET avg_rate = %s WHERE id = %s", [total_rate, id])
    mysql.connection.commit()
    cur.close()
    flash('La critique du produit a été supprimée avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer tous les avis produits

@app.route('/admin/delete_all_review_products', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_review_products():
    cur = mysql.connection.cursor()
    cur.execute("TRUNCATE t_avis_produit")
    cur.execute("UPDATE t_produit SET avg_rate = '' ")
    mysql.connection.commit()
    cur.close()
    flash('Tous les avis sur les produits ont été supprimés avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# formulaire de validation des produits slider

class AddProductsliderForm(Form):
    product_name = StringField('Nom du produit', [validators.InputRequired(), validators.length(min=1, max=180)])
    description = TextAreaField('Description', [validators.InputRequired()])
    price = IntegerField('Prix', [validators.InputRequired()])
    discount = StringField('Pourcentage de remise')
    quantity = StringField('Quantite', [validators.InputRequired()])

# admin ajouter une nouvelle page de produit de curseur

@app.route('/admin/add_product_slider', methods=['post', 'get'])
@is_admin_logged_in
def add_product_slider():
    form = AddProductsliderForm(request.form)
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT category FROM t_categorie")
    categories = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vu"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vu'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['En attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['En attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    if result > 0:
        if request.method == 'POST' and form.validate():
            product_name = form.product_name.data

            folder = os.path.exists(app.root_path + "/static/uploads/slider_products/{}".format(product_name))
            if folder == True :
                flash('Le nom du dossier existe déjà', 'warning')
                return redirect(url_for('add_product_slider'))
            cur = mysql.connection.cursor()
            cur.execute("SELECT product_name FROM t_produit_slider WHERE product_name = BINARY %s", [product_name])
            res = cur.fetchone()
            if product_name in str(res):
                msg = "Le nom du produit existe déjà"
                return render_template('admin_add_production_slider.html', form=form, msg=msg)
            slider_result = cur.execute("SELECT * FROM t_produit_slider")
            if slider_result < 3:

                file = request.files['file']
                if file.filename == '':
                    flash('Vous devez sélectionner un fichier!', 'warning')
                if file and allowed_file(file.filename):
                    try:
                        rmtree(app.root_path + "/static/uploads/slider_products/{}".format(product_name))
                        os.makedirs(app.root_path + "/static/uploads/slider_products/{}".format(product_name))
                    except:
                        os.makedirs(app.root_path + "/static/uploads/slider_products/{}".format(product_name))
                    filename = secure_filename(file.filename)
                    dir = app.root_path + "/static/uploads/slider_products/{}".format(product_name)
                    file.save(os.path.join(dir, filename))
                    category = request.form['categories']
                    description = form.description.data.lower()
                    price = form.price.data
                    discount = form.discount.data
                    quantity = form.quantity.data

                    if discount != '' and discount != ' ':
                        p = round((float(price) * float(discount)) / 100, 2)
                        cur = mysql.connection.cursor()
                        cur.execute("INSERT INTO t_produit_slider(category, product_name, description, price, discount, quantity, files)\
                                                     VALUES(%s, %s, %s, %s, %s, %s, %s)", \
                                    (category, product_name, description, price, p, quantity, filename))
                        mysql.connection.commit()
                        cur.close()
                        flash('Votre produit est publié sur le curseur avec succès!', 'success')
                        return redirect(url_for('admin_dashboard'))

                    if discount == "" or discount == " ":
                        p = 0
                        cur = mysql.connection.cursor()
                        cur.execute("INSERT INTO t_produit_slider(category, product_name, description, price, discount, quantity, files)\
                                                     VALUES(%s, %s, %s, %s, %s, %s, %s)", \
                                    (category, product_name, description, price, p, quantity, filename))
                        mysql.connection.commit()
                        cur.close()
                        flash('Votre produit est publié avec succès sur le curseur!', 'success')
                        return redirect(url_for('admin_dashboard'))
            else:
                flash('Vous ne pouvez pas ajouter plus de 3 produits dans le curseur!', 'warning')
                return redirect(url_for('admin_dashboard'))
    elif result == 0:
        flash('Créez dabord une catégorie pour ajouter un nouveau produit de curseur', 'warning')
        return redirect(url_for('admin_dashboard'))
    return render_template('admin_add_production_slider.html', form=form, categories=categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin modifier la page produit du curseur

@app.route('/admin/edit_product_slider/<id>', methods=['post', 'get'])
@is_admin_logged_in
def edit_product_slider(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT category FROM t_categorie")
    categories = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vu"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vu'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['En attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['En attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()

    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit_slider WHERE id={}".format(id))
    product = cur.fetchone()
    cur.close()
    form = AddProductForm(request.form)
    form.product_name.data = product['product_name']
    form.description.data = product['description']
    form.price.data = product['price']
    form.quantity.data = product['quantity']

    d = round((float(product['discount']) * float(100)) / float(form.price.data), 2)
    form.discount.data = d

    if request.method == 'POST' and form.validate():
        product_name = request.form['product_name']


        folder = os.path.exists(app.root_path + "/static/uploads/slider_products/{}".format(product_name))
        if folder is True and form.product_name.data == product_name:
            pass
        elif folder is False and form.product_name.data != product_name:
            pass
        else:
            flash('Le nom du dossier existe déjà', 'warning')
            return redirect(request.url)


        file = request.files['file']
        if file and allowed_file(file.filename):
            rmtree(app.root_path + "/static/uploads/slider_products/{}".format(product['product_name']))
            os.makedirs(app.root_path + "/static/uploads/slider_products/{}".format(product_name))
            filename = secure_filename(file.filename)
            dir = app.root_path + "/static/uploads/slider_products/{}".format(product_name)
            file.save(os.path.join(dir, filename))

            category = request.form['categories']
            description = request.form['description']
            price = request.form['price']
            discount = request.form['discount']
            quantity = request.form['quantity']

            if discount == "" or discount == " ":
                p = 0
                cur = mysql.connection.cursor()
                cur.execute("UPDATE t_produit_slider SET category=%s, product_name=%s, description=%s, price=%s,\
                             discount=%s, quantity=%s, files=%s WHERE id=%s", \
                            (category, product_name, description, price, p, quantity, filename, id))
                mysql.connection.commit()
                cur.close()
                flash('Votre produit slider a été modifié avec succès!', 'success')
                return redirect(url_for('admin_dashboard'))
            
            p = round((float(price) * float(discount)) / 100, 2)

            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_produit_slider SET category=%s, product_name=%s, description=%s, price=%s,\
                         discount=%s, quantity=%s, files=%s WHERE id=%s", \
                        (category, product_name, description, price, p, quantity, filename, id))
            mysql.connection.commit()
            cur.close()
            flash('Votre produit slider a été modifié avec succès!', 'success')
            return redirect(url_for('admin_dashboard'))
        elif file.filename == '' or 'file' not in request.files:
            os.rename(os.path.join(app.root_path + "/static/uploads/slider_products/{}".format(product['product_name'])),
                      os.path.join(app.root_path + "/static/uploads/slider_products/{}".format(product_name)))
            category = request.form['categories']
            description = request.form['description']
            price = request.form['price']
            discount = request.form['discount']
            quantity = request.form['quantity']

            if discount == "" or discount == " ":
                p = 0
                cur = mysql.connection.cursor()
                cur.execute("UPDATE t_produit_slider SET category=%s, product_name=%s, description=%s, price=%s,\
                             discount=%s, quantity=%s WHERE id=%s", \
                            (category, product_name, description, price, p, quantity, id))
                mysql.connection.commit()
                cur.close()
                flash('Votre produit slider a été modifié avec succès!', 'success')
                return redirect(url_for('admin_dashboard'))
            
            p = round((float(price) * float(discount)) / 100, 2)
            
            
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_produit_slider SET category=%s, product_name=%s, description=%s, price=%s,\
                         discount=%s, quantity=%s WHERE id=%s", \
                        (category, product_name, description, price, p, quantity, id))
            mysql.connection.commit()
            cur.close()
            flash('Votre produit slider a été modifié avec succès!', 'success')
            return redirect(url_for('admin_dashboard'))
    return render_template('admin_edit_production_slider.html', form=form, categories=categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin supprimer le produit du curseur

@app.route('/admin/delete_product_slider/<id>', methods=['post', 'get'])
@is_admin_logged_in
def delete_product_slider(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT product_name FROM t_produit_slider WHERE id = %s", [id])
    name = cur.fetchone()
    n = name['product_name']
    try:
        rmtree(app.root_path + "/static/uploads/slider_products/{}".format(n))
    except:
        pass
    cur.execute("DELETE FROM t_produit_slider WHERE id = %s", [id])
    cur.execute("DELETE FROM t_avis_produit_slider WHERE product_id = %s", [id])
    mysql.connection.commit()
    cur.close()
    flash('Votre produit slider a été supprimé avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer tous les produits de curseur

@app.route('/admin/delete_all_slider_products', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_slider_products():
    cur = mysql.connection.cursor()
    cur.execute("TRUNCATE t_produit_slider")
    cur.execute("TRUNCATE t_avis_produit_slider")
    mysql.connection.commit()
    cur.close()
    try:
        rmtree(app.root_path + "/static/uploads/slider_products")
        flash('Vous avez été supprimé avec succès tous les produits!', 'success')
    except:
        flash('Vous avez déjà supprimé tous les produits avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer le curseur produit examen

@app.route('/admin/delete_review_slider_product/<id>/<id2>', methods=['post', 'get'])
@is_admin_logged_in
def delete_review_slider_product(id, id2):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_avis_produit_slider WHERE product_id = %s AND id = %s", [id, id2])
    mysql.connection.commit()
    cur.execute("SELECT SUM(rate) / COUNT(product_id) AS total_avg_rate FROM t_avis_produit_slider WHERE product_id = %s", [id])
    total = cur.fetchone()
    total_rate = total['total_avg_rate']
    cur.execute("UPDATE slider_products SET avg_rate = %s WHERE id = %s", [total_rate, id])
    mysql.connection.commit()
    cur.close()
    flash('La critique du produit Slider a été supprimée avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer tous les avis produits

@app.route('/admin/delete_all_slider_products_reviews', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_slider_products_reviews():
    cur = mysql.connection.cursor()
    cur.execute("TRUNCATE t_avis_produit_slider")
    cur.execute("UPDATE t_produit_slider SET avg_rate = '' ")
    mysql.connection.commit()
    cur.close()
    flash('Tous les avis sur les produits de curseur ont été supprimés avec succès', 'success')
    return redirect(url_for('admin_dashboard'))

# admin créer un formulaire de validation de compte utilisateur

class AdduserForm(Form):
    first_name = StringField('Prénom', [validators.InputRequired()])
    last_name = StringField('Nom', [validators.InputRequired()])
    username = StringField('Pseudo', [validators.InputRequired()])
    password = PasswordField('mot de passe',
                             [validators.DataRequired(), validators.Length(min=6, max=100),
                              validators.EqualTo('Confirmer', message='Les mots de passe ne correspondent pas')])
    confirm = PasswordField('Confirmez le mot de passe', [validators.DataRequired()])

# admin créer une page de compte d'utilisateur

@app.route('/admin/add_user', methods=['post', 'get'])
@is_admin_logged_in
def add_user():
    cur = mysql.connection.cursor()
    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vu"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vu'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['En attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['En attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    form = AdduserForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data

        folder = os.path.exists(app.root_path + "/static/uploads/users/{}".format(username))
        if folder == True:
            flash('Le nom du dossier existe déjà', 'warning')
            return redirect(url_for('add_user'))


        cur = mysql.connection.cursor()
        cur.execute("SELECT username FROM t_utilisateurs WHERE username = %s", [username])
        res = cur.fetchone()
        cur.close()
        if username in str(res):
            msg = "Ce nom d'utilisateur existe déjà"
            return render_template('admin_add_user.html', form=form, msg=msg, admin_name=session['admin_username'], admin_image=session['admin_image'])
        else:
            permission = request.form['permissions']
            first_name = form.first_name.data.lower()
            last_name = form.last_name.data.lower()
            email = request.form['email'].lower()
            gender = request.form['gender']
            country = request.form['country']
            username = form.username.data
            password = sha256_crypt.encrypt(str(form.password.data))
            file = request.files['file']
            try:
                rmtree(app.root_path + "/static/uploads/users/{}".format(username))
                os.makedirs(app.root_path + "/static/uploads/users/{}".format(username))
            except:
                os.makedirs(app.root_path + "/static/uploads/users/{}".format(username))
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                dir = app.root_path + "/static/uploads/users/{}".format(username)
                file.save(os.path.join(dir, filename))
                cur = mysql.connection.cursor()
                cur.execute("INSERT INTO t_utilisateurs(permission, first_name, last_name,\
                             email, gender, country, username, password, reset_password_permission, files)\
                             VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                            (permission, first_name, last_name, email, gender,\
                             country, username, password, 'no_reset', filename))
                mysql.connection.commit()
                cur.close()
                flash('Vous avez créé un compte avec succès!', 'success')
                return redirect(url_for('admin_dashboard'))
            elif file.filename == '' or 'file' not in request.files:
                copy(app.root_path + '/static/admin.png', app.root_path + '/static/uploads/users/{}/admin.png'.format(username))
                cur = mysql.connection.cursor()
                cur.execute("INSERT INTO t_utilisateurs(permission, first_name, last_name,\
                                             email, gender, country, username, password, reset_password_permission, files)\
                                             VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                            (permission, first_name, last_name, email, gender, \
                             country, username, password, 'no_reset', 'admin.png'))
                mysql.connection.commit()
                cur.close()
                flash('Vous avez créé un compte avec succès!', 'success')
                return redirect(url_for('admin_dashboard'))
    return render_template('admin_add_user.html', form=form, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin supprimer l'utilisateur 

@app.route('/admin/delete_user/<id>', methods=['post', 'get'])
@is_admin_logged_in
def delete_user(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT username FROM t_utilisateurs WHERE id = %s", [id])
    name = cur.fetchone()
    n = name['username']
    try:
        rmtree(app.root_path + "/static/uploads/users/{}".format(n))
    except:
        pass
    cur.execute("DELETE FROM t_commande WHERE user_name = %s", [n])
    cur.execute("DELETE FROM t_achat_commande WHERE user_name = %s", [n])
    cur.execute("DELETE FROM t_avis_produit WHERE user_name = %s", [n])
    cur.execute("DELETE FROM t_avis_produit_slider WHERE user_name = %s", [n])
    cur.execute("DELETE FROM t_utilisateurs WHERE id = %s", [id])
    mysql.connection.commit()
    cur.close()
    flash('Vous avez supprimé le compte utilisateur avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin ajouter un nouveau formulaire de validation de catégorie

class CategoryForm(Form):
    category = StringField('Category', [validators.InputRequired(), validators.length(min=1, max=100)])
    
# admin ajouter une nouvelle page de catégorie

@app.route('/admin/add_category', methods=['post', 'get'])
@is_admin_logged_in
def add_category():
    cur = mysql.connection.cursor()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vu"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vu'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['En attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['En attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    form = CategoryForm(request.form)
    if request.method == 'POST' and form.validate():
        category = form.category.data.lower()
        cur = mysql.connection.cursor()
        result = cur.execute("SELECT * FROM t_categorie WHERE category = BINARY %s", [category])
        if result > 0:
            cur.close()
            flash('Cette catégorie existe déjà', 'warning')
            return redirect(url_for('admin_dashboard'))
        if category == ' ':
            cur.close()
            flash('Vous devez taper un mot!', 'warning')
            return redirect(url_for('add_category'))
        if result == 0:
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO t_categorie (category) VALUES(%s);", ([category]))
            mysql.connection.commit()
            cur.close()
            flash('Vous avez ajouté une nouvelle catégorie avec succès!', 'success')
            return redirect(url_for('admin_dashboard'))
    return render_template('admin_add_category.html', form=form, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# page de modification de la catégorie admin

@app.route('/admin/edit_category/<current_category>', methods=['post', 'get'])
@is_admin_logged_in
def edit_category(current_category):
        cur = mysql.connection.cursor()
        cur.execute("SELECT category FROM t_categorie Where category=%s;", [current_category])
        cat = cur.fetchone()

        # afficher les messages
        cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vu"])
        messages = cur.fetchall()

        # afficher le numéro des messages
        cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vu'])
        count_message = cur.fetchone()
        count_messages = count_message['COUNT(id)']

        # afficher le nouveau numéro de commande
        cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['En attente'])
        count_order = cur.fetchone()
        count_orders_where_pending = count_order['COUNT(status)']

        # afficher les nouvelles commandes
        cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['En attente'])
        count_orders_by_user = cur.fetchall()

        cur.close()
        form = CategoryForm(request.form)
        form.category.data = cat['category']
        if request.method == 'POST' and form.validate():
            category = request.form['category'].lower()
            if category == ' ':
                cur.close()
                flash('Vous devez taper un mot!', 'warning')
                return redirect(url_for('add_category'))
            cur = mysql.connection.cursor()
            cur.execute("UPDATE t_categorie SET category=%s WHERE category=%s;", ([category], [current_category]))
            cur.execute("UPDATE t_produit SET category=%s WHERE category=%s", \
                        ([category], [current_category]))
            cur.execute("UPDATE t_produit_slider SET category=%s WHERE category=%s", \
                        ([category], [current_category]))
            mysql.connection.commit()
            cur.close()
            flash('Vous avez mise a jour la catégorie avec ses produits avec succès!', 'success')
            return redirect(url_for('admin_dashboard'))
        return render_template('admin_edit_category.html', form=form, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin supprimer la catégorie 

@app.route('/admin/delete_category/<category>', methods=['post', 'get'])
@is_admin_logged_in
def delete_category(category):
        cur = mysql.connection.cursor()
        prod = cur.execute("SELECT product_name FROM t_produit WHERE category=%s", [category])
        if prod > 0:
            pass
        products = cur.fetchall()
        for product in products:
            rmtree(app.root_path + "/static/uploads/products/{}".format(product['product_name']))
            cur.execute("DELETE FROM t_avis_produit WHERE product_name=%s", [product['product_name']])
            mysql.connection.commit()

        slider = cur.execute("SELECT product_name FROM t_produit_slider WHERE category=%s", [category])
        if slider > 0:
            pass
        sliders = cur.fetchall()
        for slider in sliders:
            rmtree(app.root_path + "/static/uploads/slider_products/{}".format(slider['product_name']))
            cur.execute("DELETE FROM t_avis_produit_slider WHERE product_name=%s", [slider['product_name']])
            mysql.connection.commit()

        cur.execute("DELETE FROM t_produit_slider WHERE category=%s", [category])
        cur.execute("DELETE FROM t_produit WHERE category=%s", [category])
        cur.execute("DELETE FROM t_categorie Where category=%s;", [category])
        mysql.connection.commit()
        cur.close()        
        flash("Vous avez supprimé la catégorie avec ses produits avec succès!", 'success')
        return redirect(url_for('admin_dashboard'))

# admin supprimer toutes les catégories

@app.route('/admin/delete_all_categories', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_categories():
    cur = mysql.connection.cursor()
    cur.execute("TRUNCATE t_categorie")
    cur.execute("TRUNCATE t_produit")
    cur.execute("TRUNCATE t_produit_slider")
    cur.execute("TRUNCATE t_commande")
    cur.execute("TRUNCATE t_achat_commande")
    cur.execute("TRUNCATE t_avis_produit")
    cur.execute("TRUNCATE t_avis_produit_slider")
    mysql.connection.commit()
    cur.close()
    try:
        rmtree(app.root_path + "/static/uploads/products")
        rmtree(app.root_path + "/static/uploads/slider_products")
        flash('Vous avez supprimé toutes les catégories et tous les produits avec succès!', 'success')
    except:
        flash('Vous avez supprimé toutes les catégories et tous les produits avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer tous les utilisateurs

@app.route('/admin/delete_all_users', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_users():
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT username FROM t_utilisateurs WHERE permission = 'user'")
    if result >0:
        name = cur.fetchall()
        for n in name:
            rmtree(app.root_path + "/static/uploads/users/{}".format(n['username']))
    elif result == 0:
        pass
    cur.execute("DELETE FROM t_utilisateurs WHERE permission = 'user' ")
    mysql.connection.commit()
    cur.close()
    flash('Vous avez supprimé tous les comptes dutilisateurs avec leurs fichiers avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin supprimer tous les comptes

@app.route('/admin/delete_all_accounts', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_accounts():
    try:
        rmtree(app.root_path + "/static/uploads/users")
        rmtree(app.root_path + "/static/uploads/products")
        rmtree(app.root_path + "/static/uploads/slider_products")
    except:
        pass
    cur = mysql.connection.cursor()
    cur.execute("TRUNCATE t_utilisateurs")
    cur.execute("TRUNCATE t_categorie")
    cur.execute("TRUNCATE t_produit")
    cur.execute("TRUNCATE t_produit_slider")
    cur.execute("TRUNCATE t_commande")
    cur.execute("TRUNCATE t_achat_commande")
    cur.execute("TRUNCATE t_avis_produit")
    cur.execute("TRUNCATE t_avis_produit_slider")
    mysql.connection.commit()
    cur.close()
    session.clear()
    flash('Vous avez supprimé tous les comptes avec leurs fichiers avec succès!', 'success')
    return redirect(url_for('admin_login'))

# l'administrateur accepte les commandes

@app.route('/admin/accept_orders/<id>', methods=['post', 'get'])
@is_admin_logged_in
def accept_orders(id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s WHERE id = %s", (['Accepté'], id))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez accepté la commande avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# l'administrateur accepte toutes les commandes

@app.route('/admin/accept_all_orders', methods=['post', 'get'])
@is_admin_logged_in
def accept_all_orders():
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s", (['Accepted']))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez accepté toutes les commandes avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# l'administrateur rejette les commandes

@app.route('/admin/reject_orders/<id>', methods=['post', 'get'])
@is_admin_logged_in
def reject_orders(id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s WHERE id = %s", (['Rejected'], id))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez rejeté la commande avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# l'administrateur rejette toutes les commandes

@app.route('/admin/reject_all_orders', methods=['post', 'get'])
@is_admin_logged_in
def reject_all_orders():
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s", (['Rejected']))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez rejeté toutes les commandes avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# barre de recherche admin

@app.route('/search', methods=['GET', 'POST'])
@is_admin_logged_in
def search():
    if request.method == "POST":
        cur = mysql.connection.cursor()
        # afficher les messages
        cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
        messages = cur.fetchall()

        # afficher le numéro des messages
        cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
        count_message = cur.fetchone()
        count_messages = count_message['COUNT(id)']

        # afficher le nouveau numéro de commande
        cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
        count_order = cur.fetchone()
        count_orders_where_pending = count_order['COUNT(status)']

        # afficher les nouvelles commandes
        cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
        count_orders_by_user = cur.fetchall()

        result = cur.execute("SELECT * FROM t_produit \
                             WHERE( CONVERT(`product_name` USING utf8)\
                             LIKE %s)", [["%" + request.form['search'] + "%"]])
        search_products = cur.fetchall()
        cur.close()
        if result > 0:
            return render_template('admin_search.html', search_products=search_products, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)
        else:
            flash('Aucun produit trouvé!', 'warning')
            return redirect(url_for('admin_dashboard'))

# page d'aperçu de tous les produits de curseur

@app.route('/admin/slider_products_table')
@is_admin_logged_in
def slider_products_table():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit_slider")
    slider_products = cur.fetchall()
    cur.execute("SELECT COUNT(id) FROM t_produit_slider")
    sliders = cur.fetchone()
    count_sliders = sliders['COUNT(id)']

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_slider_products_table .html', slider_products=slider_products, count_sliders=count_sliders, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin aperçu de la page du tableau de tous les produits

@app.route('/admin/products_table')
@is_admin_logged_in
def products_table():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit")
    products = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # show messages number
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['Pending'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['Pending'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_products_table.html', products=products, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin aperçu de la page du tableau toutes les catégories

@app.route('/admin/categories_table')
@is_admin_logged_in
def categories_table():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_categorie")
    categories = cur.fetchall()

    cur.execute("SELECT COUNT(category) FROM t_categorie")
    category = cur.fetchone()
    count_categories = category['COUNT(category)']
    cur.execute("SELECT COUNT(id) FROM t_produit")
    products = cur.fetchone()
    count_products = products['COUNT(id)']

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_categories_table.html', categories=categories, count_products=count_products, count_categories=count_categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# afficher tous les produits dans une catégorie spécifique

@app.route('/admin/categories/<category>', methods=['post', 'get'])
@is_admin_logged_in
def admin_categories(category):
    cur = mysql.connection.cursor()
    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.execute("SELECT category, number_of_products FROM t_categorie")
    all_categories = cur.fetchall()
    result = cur.execute("SELECT * FROM t_produit WHERE category=%s", [category])
    categories = cur.fetchall()
    cur.close()
    if result > 0:
        return render_template('admin_catigories.html', category=category, categories=categories, all_categories=all_categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)
    else:
        msg = 'Aucun produit trouvé!'
        return render_template('admin_catigories.html', msg=msg, category=category, all_categories=all_categories, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin aperçu de la page du tableau de tous les utilisateurs

@app.route('/admin/users_table')
@is_admin_logged_in
def users_table():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_utilisateurs")
    users = cur.fetchall()
    cur.execute("SELECT COUNT(username) FROM t_utilisateurs WHERE permission = 'user'")
    count_userss = cur.fetchone()
    count_users = count_userss['COUNT(username)']

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_users_table.html', users=users, count_users=count_users, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# admin aperçu de la page du tableau de tous les utilisateurs

@app.route('/admin/orders_table')
@is_admin_logged_in
def orders_table():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_achat_commande ;")
    orders = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_orders_table.html', orders=orders, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# page de révision des produits par l'administrateur

@app.route('/admin/review_products')
@is_admin_logged_in
def review_products():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_avis_produit")
    review_products = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_products_reviews.html', review_products=review_products, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# page de révision des produits par l'administrateur

@app.route('/admin/review_slider_products')
@is_admin_logged_in
def review_slider_products():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_avis_produit_slider")
    review_slider_products = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_slider_products_reviews.html', review_slider_products=review_slider_products, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# produit d'aperçu administrateur

@app.route('/admin/product/<id>')
@is_admin_logged_in
def product(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit WHERE id = %s", [id])
    product = cur.fetchone()
    reviewresult = cur.execute("SELECT * FROM t_avis_produit WHERE product_id={} ORDER BY id DESC limit 1".format(id))
    review = cur.fetchall()
    cur.execute("SELECT SUM(rate) / COUNT(product_name) AS avg_rate FROM t_avis_produit WHERE product_id = %s;", [id])
    rate = cur.fetchone()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_product.html', product=product, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], reviewresult=reviewresult, review=review, rate=rate, messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# produit d'aperçu administrateur

@app.route('/admin/slider/<id>')
@is_admin_logged_in
def slider(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_produit_slider WHERE id = %s", [id])
    product = cur.fetchone()
    reviewresult = cur.execute("SELECT * FROM t_avis_produit_slider WHERE product_id={} ORDER BY id DESC limit 1".format(id))
    review = cur.fetchall()
    cur.execute("SELECT SUM(rate) / COUNT(product_name) AS avg_rate FROM t_avis_produit_slider WHERE product_id = %s;", [id])
    rate = cur.fetchone()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_slider.html', product=product, admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], reviewresult=reviewresult, review=review, rate=rate, messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# afficher la page du tableau des messages

@app.route('/admin/messages_table', methods=['post', 'get'])
@is_admin_logged_in
def admin_messages_table():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_contacter_nous ;")
    all_messages = cur.fetchall()
    cur.execute("SELECT status FROM t_contacter_nous WHERE status = %s ;", ['vue'])
    seen_messages = cur.fetchall()
    cur.execute("SELECT status FROM t_contacter_nous WHERE status = %s ;", ['pas vue'])
    not_seen_messages = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_messages_table.html', admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], all_messages=all_messages, seen_messages=seen_messages, not_seen_messages=not_seen_messages, messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# afficher la page du message

@app.route('/admin/message/<id>', methods=['post', 'get'])
@is_admin_logged_in
def admin_message(id):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_contacter_nous WHERE id = %s ;", [id])
    current_message = cur.fetchone()
    cur.execute("UPDATE t_contacter_nous SET status = %s WHERE id = %s", (['vue'], id))
    mysql.connection.commit()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_message.html', admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], current_message=current_message, messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# supprimer le message

@app.route('/admin/delete_message/<id>', methods=['post', 'get'])
@is_admin_logged_in
def admin_delete_message(id):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_contacter_nous WHERE id = %s ;", [id])
    mysql.connection.commit()
    cur.close()
    flash('Vous avez réussi à supprimer le message!', 'success')
    return redirect(url_for('admin_dashboard'))

# supprimer tous les messages

@app.route('/admin/delete_all_messages', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_messages():
    cur = mysql.connection.cursor()
    cur.execute("TRUNCATE t_contacter_nous")
    mysql.connection.commit()
    cur.close()
    flash('Vous avez réussi à supprimer tous les messages!', 'success')
    return redirect(url_for('admin_dashboard'))

# supprimer tous les messages vus

@app.route('/admin/delete_all_seen_messages', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_seen_messages():
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_contacter_nous WHERE status = %s ", ['vue'])
    mysql.connection.commit()
    cur.close()
    flash('Vous avez réussi à supprimer tous les messages vus!', 'success')
    return redirect(url_for('admin_dashboard'))

# supprimer tous les messages non vus

@app.route('/admin/delete_all_not_seen_messages', methods=['post', 'get'])
@is_admin_logged_in
def delete_all_not_seen_messages():
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    mysql.connection.commit()
    cur.close()
    flash('Vous avez réussi à supprimer tous les messages non vus!', 'success')
    return redirect(url_for('admin_dashboard'))

# admin afficher les détails des commandes pour l'utilisateur

@app.route('/admin/show_orders/<username>', methods=['post', 'get'])
@is_admin_logged_in
def show_orders(username):
    session['orders_username'] = username
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM t_achat_commande WHERE user_name = %s ;", [username])
    orders = cur.fetchall()

    # afficher les messages
    cur.execute("SELECT * FROM t_contacter_nous WHERE status = %s ORDER BY id DESC LIMIT 6;", ["pas vue"])
    messages = cur.fetchall()

    # afficher le numéro des messages
    cur.execute("SELECT COUNT(id) FROM t_contacter_nous WHERE status = %s ", ['pas vue'])
    count_message = cur.fetchone()
    count_messages = count_message['COUNT(id)']

    # afficher le nouveau numéro de commande
    cur.execute("SELECT COUNT(status) FROM t_achat_commande WHERE status = %s", ['en attente'])
    count_order = cur.fetchone()
    count_orders_where_pending = count_order['COUNT(status)']

    # afficher les nouvelles commandes
    cur.execute("SELECT COUNT(status), user_name FROM t_achat_commande WHERE status = %s GROUP BY user_name ASC LIMIT 12", ['en attente'])
    count_orders_by_user = cur.fetchall()

    cur.close()
    return render_template('admin_show_orders_by_user_table.html', admin_name=session['admin_username'], admin_image=session['admin_image'], permission=session['permission'], orders=orders, messages=messages, count_messages=count_messages, count_orders_where_pending=count_orders_where_pending, count_orders_by_user=count_orders_by_user)

# l'administrateur accepte les commandes de l'utilisateur

@app.route('/admin/accept_order_user/<username>/<id>', methods=['post', 'get'])
@is_admin_logged_in
def accept_order_user(username, id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s WHERE id = %s AND user_name = %s", (['Accepted'], id, username))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez accepté la commande avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# l'administrateur accepte toutes les commandes de l'utilisateur

@app.route('/admin/accept_all_orders_user/<username>', methods=['post', 'get'])
@is_admin_logged_in
def accept_all_orders_user(username):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s WHERE user_name = %s ", (['Accepted'], username))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez accepté toutes les commandes avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# l'administrateur rejette les commandes de l'utilisateur

@app.route('/admin/reject_order_user/<username>/<id>', methods=['post', 'get'])
@is_admin_logged_in
def reject_order_user(username, id):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s WHERE id = %s AND user_name = %s", (['Rejected'], id, username))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez rejeté la commande avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# l'administrateur rejette toutes les commandes de l'utilisateur

@app.route('/admin/reject_all_orders_user/<username>', methods=['post', 'get'])
@is_admin_logged_in
def reject_all_orders_user(username):
    cur = mysql.connection.cursor()
    cur.execute("UPDATE t_achat_commande SET status = %s WHERE user_name = %s", (['Rejected'], username))
    mysql.connection.commit()
    cur.close()
    flash('Vous avez rejeté toutes les commandes avec succès!', 'success')
    return redirect(url_for('admin_dashboard'))

# exécuter toute la fonction d'application

if __name__ == '__main__':
 app.config['SECRET_KEY'] = 'epsic_maccaud_module_104'